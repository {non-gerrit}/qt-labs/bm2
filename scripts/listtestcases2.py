import sys
import json
from dbaccess import execQuery
from misc import (
    idToText, textToId, getContext, benchmarkToComponents, printJSONHeader)

class ListTestCases2:

    def __init__(
        self, host1, platform1, branch1, sha11, host2, platform2, branch2,
        sha12):
        self.host1 = host1
        self.platform1 = platform1
        self.branch1 = branch1
        self.sha11 = sha11
        self.host2 = host2
        self.platform2 = platform2
        self.branch2 = branch2
        self.sha12 = sha12
        self.host1_id = textToId("host", host1)
        self.platform1_id = textToId("platform", platform1)
        self.branch1_id = textToId("branch", branch1)
        self.sha11_id = textToId("sha1", sha11)
        self.host2_id = textToId("host", host2)
        self.platform2_id = textToId("platform", platform2)
        self.branch2_id = textToId("branch", branch2)
        self.sha12_id = textToId("sha1", sha12)

    def execute(self):

        # Get all distinct benchmarks matching both contexts:
        bmark_ids = execQuery(
            "SELECT DISTINCT benchmarkId"
            " FROM result WHERE contextId = %s"
            " INTERSECT "
            "SELECT DISTINCT benchmarkId"
            " FROM result WHERE contextId = %s",
            (getContext(
                    self.host1_id, self.platform1_id, self.branch1_id,
                    self.sha11_id),
             getContext(
                    self.host2_id, self.platform2_id, self.branch2_id,
                    self.sha12_id))
            )

        # Extract all distinct test case components:
        tc_map = {}
        for item in bmark_ids:
            bmark_id = item[0]
            benchmark = idToText("benchmark", bmark_id)
            test_case, test_function, data_tag = (
                benchmarkToComponents(benchmark))
            tc_map[test_case] = True

        self.test_cases = sorted(tuple(tc_map.keys()))
        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'testcases': self.test_cases }, sys.stdout)

class ListTestCases2AsJSON(ListTestCases2):
    def writeOutput(self):
        self.writeOutputAsJSON()
