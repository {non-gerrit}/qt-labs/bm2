import sys, json
from dbaccess import execQuery
from misc import printJSONHeader


class GetNameMappings:

    def __init__(self):
        pass

    def execute(self):
        self.hosts = dict(execQuery("SELECT id, value FROM host", ()))
        self.platforms = dict(execQuery("SELECT id, value FROM platform", ()))
        self.branches = dict(execQuery("SELECT id, value FROM branch", ()))
        self.sha1s = dict(execQuery("SELECT id, value FROM sha1", ()))
        self.benchmarks = dict(execQuery("SELECT id, value FROM benchmark", ()))
        self.metrics = dict(execQuery("SELECT id, value FROM metric", ()))
        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({
                'hosts': self.hosts,
                'platforms': self.platforms,
                'branches': self.branches,
                'sha1s': self.sha1s,
                'benchmarks': self.benchmarks,
                'metrics': self.metrics
                }, sys.stdout)


class GetNameMappingsAsJSON(GetNameMappings):
    def writeOutput(self):
        self.writeOutputAsJSON()
