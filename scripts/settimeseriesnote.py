import sys
import json
from dbaccess import execQuery, database, commit
from misc import textToId, printJSONHeader

class SetTimeSeriesNote:

    def __init__(
        self, host, platform, branch, benchmark, metric, note):
        self.host_id = textToId('host', host)
        self.platform_id = textToId('platform', platform)
        self.branch_id = textToId('branch', branch)
        self.benchmark_id = textToId('benchmark', benchmark)
        self.metric_id = textToId('metric', metric)
        max_length = 256
        self.note = note.strip()[:max_length]

    def execute(self):

        if len(self.note) == 0:
            # Delete the row from the table:
            execQuery(
                "DELETE FROM timeSeriesAnnotation"
                " WHERE hostId = %s"
                "   AND platformId = %s"
                "   AND branchId = %s"
                "   AND benchmarkId = %s"
                "   AND metricId = %s",
                (self.host_id, self.platform_id, self.branch_id,
                 self.benchmark_id, self.metric_id),
                False)
        else:
            # Insert or update table row:
            execQuery(
                "SELECT merge_timeSeriesNote(%s, %s, %s, %s, %s, %s)",
                (self.host_id, self.platform_id, self.branch_id,
                 self.benchmark_id, self.metric_id, self.note),
                False)

        # Write to database:
        commit()

        # Return empty reply to client:
        printJSONHeader()
        json.dump({ }, sys.stdout)
