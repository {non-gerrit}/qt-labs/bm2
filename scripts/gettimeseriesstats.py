import sys
import json
from dbaccess import database
from misc import (
    idToText, textToId, metricIdToLowerIsBetter, benchmarkToComponents,
    getSnapshots, getBMTimeSeriesStatsList, printJSONHeader)

class GetTimeSeriesStats:

    def __init__(
        self, host, platform, branch, sha11, sha12, difftol, durtolmin,
        durtolmax, test_case_filter):
        self.host = host
        self.platform = platform
        self.branch = branch
        self.sha11 = sha11
        self.sha12 = sha12
        self.difftol = difftol
        self.durtolmin = durtolmin
        self.durtolmax = durtolmax
        self.test_case_filter = test_case_filter
        self.host_id = textToId("host", host)
        self.platform_id = textToId("platform", platform)
        self.branch_id = textToId("branch", branch)
        self.sha11_id = textToId("sha1", sha11)
        self.sha12_id = textToId("sha1", sha12)

    def execute(self):
        self.snapshots = getSnapshots(
            self.host_id, self.platform_id, self.branch_id, self.sha11_id,
            self.sha12_id)
        bmstats_list = getBMTimeSeriesStatsList(
            self.host_id, self.platform_id, self.branch_id, self.snapshots,
            self.test_case_filter, self.difftol, self.durtolmin, self.durtolmax)

        self.per_bm_stats = []
        for stats in bmstats_list:

            self.per_bm_stats.append({
                    'ms': stats['ms'],
                    'lsd': stats['lsd'],
                    'ni': stats['ni'],
                    'nz': stats['nz'],
                    'nc': stats['nc'],
                    'med_of_rses': stats['med_of_rses'],
                    'rse_of_meds': stats['rse_of_meds'],
                    'lc': stats['lc'],
                    'lc_timestamp': stats['lc_timestamp'],
                    'lc_distance': stats['lc_distance'],
                    'lc_gsep_score': stats['lc_gsep_score'],
                    'lc_lsep_score': stats['lc_lsep_score'],
                    'lc_dur1_score': stats['lc_dur1_score'],
                    'lc_dur2_score': stats['lc_dur2_score'],
                    'mt': stats['metric'],
                    'lib': "1" if stats['lib'] else "0",
                    'bm': stats['benchmark']
                    })

        self.writeOutput()


    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({
                'database': database(),
                'host': self.host,
                'platform': self.platform,
                'branch': self.branch,
                'sha11': idToText("sha1", self.snapshots[0][0]),
                'sha12': idToText("sha1", self.snapshots[-1][0]),
                'difftol': self.difftol,
                'durtolmin': self.durtolmin,
                'durtolmax': self.durtolmax,
                'snapshots': map(
                    lambda s: (idToText("sha1", s[0]), s[1]), self.snapshots),
                'per_bm_stats': self.per_bm_stats
                }, sys.stdout)


class GetTimeSeriesStatsAsJSON(GetTimeSeriesStats):
    def writeOutput(self):
        self.writeOutputAsJSON()
