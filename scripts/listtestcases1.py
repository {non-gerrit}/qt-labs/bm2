import sys
import json
from dbaccess import execQuery
from misc import (
    idToText, textToId, getContext, benchmarkToComponents, printJSONHeader)

class ListTestCases1:

    def __init__(self, host, platform, branch, sha1):
        self.host = host
        self.platform = platform
        self.branch = branch
        self.sha1 = sha1
        self.host_id = textToId("host", host)
        self.platform_id = textToId("platform", platform)
        self.branch_id = textToId("branch", branch)
        self.sha1_id = textToId("sha1", sha1)

    def execute(self):

        # Get all distinct benchmarks matching the context:
        bmark_ids = execQuery(
            "SELECT DISTINCT benchmarkId "
            "FROM result WHERE contextId = %s",
            (getContext(
                    self.host_id, self.platform_id, self.branch_id,
                    self.sha1_id),))

        # Extract all distinct test case components:
        tc_map = {}
        for item in bmark_ids:
            bmark_id = item[0]
            benchmark = idToText("benchmark", bmark_id)
            test_case, test_function, data_tag = (
                benchmarkToComponents(benchmark))
            tc_map[test_case] = True

        self.test_cases = sorted(tuple(tc_map.keys()))
        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'testcases': self.test_cases }, sys.stdout)

class ListTestCases1AsJSON(ListTestCases1):
    def writeOutput(self):
        self.writeOutputAsJSON()
