#!/usr/bin/env python

from listcontexts import ListContextsAsJSON
from listtestcases1 import ListTestCases1AsJSON
from listtestcases2 import ListTestCases2AsJSON
from getstats1 import GetStats1AsJSON
from getstats2 import GetStats2AsJSON
from getresultdetails2 import GetResultDetails2AsJSON
from gettimeseriesstats import GetTimeSeriesStatsAsJSON
from gettimeseriesdetails import GetTimeSeriesDetailsAsJSON
from getsnapshots import GetSnapshotsAsJSON
from settimeseriesnote import SetTimeSeriesNote
from gettopchanges import GetTopChangesAsJSON
from getnamemappings import GetNameMappingsAsJSON
from gettestcaseswithchanges import GetTestCasesWithChangesAsJSON

from dbaccess import setDatabase
from misc import getOptions, printErrorAsJSON
import sys

# --- BEGIN Global functions ----------------------------------------------

# Returns true iff name exists in options and is true.
def boolOption(options, name):
    if name in options:
        try:
            res = (int(options[name]) != 0)
        except:
            res = (options[name].lower() == "true")
    else:
        res = False
    return res


# Returns a command instance.
def createCommand(options, http_get):

    def printUsageError():
        error = (
            "usage: " + sys.argv[0] + " [--dbhost H --dbport P] --db D + \\\n"
            "  --cmd contexts | \\\n"
            "  --cmd testcases1 --host H --platform P --branch B "
            "--sha1 S | \\\n"
            "  --cmd testcases2 --host1 H --platform1 P --branch1 B "
            "--sha11 S --host2 H --platform2 P --branch2 B --sha12 S | \\\n"
            "  --cmd stats1 --host H --platform P --branch B "
            "--sha1 S [--testcasefilter 'TC1 TC2 ...'] | \\\n"
            "  --cmd stats2 --host1 H --platform1 P --branch1 B "
            "--sha11 S --host2 H --platform2 P --branch2 B --sha12 S "
            "[--testcasefilter 'TC1 TC2 ...'] | \\\n"
            "  --cmd result_details2 --host1 H --platform1 P "
            "--branch1 B --sha11 S --host2 H --platform2 P --branch2 B "
            "--sha12 S --benchmark BM --metric M | \\\n"
            "  --cmd timeseriesstats --host H --platform P --branch B "
            "--sha11 S --sha12 S [--difftol T] [--durtolmin T] "
            "[--durtolmax T] [--testcasefilter 'TC1 TC2 ...'] | \\\n"
            "  --cmd timeseriesdetails --host H --platform P "
            "--branch B --sha11 S --sha12 S --difftol T --durtolmin T "
            "--durtolmax T --benchmark BM --metric M | \\\n"
            "  --cmd snapshots --host H --platform P "
            "--branch B --sha11 S --sha12 S | \\\n"
            "  --cmd settimeseriesnote --host H --platform P "
            "--branch B --benchmark B --metric M --note N | \\\n"
            "  --cmd topchanges --regressions R --last L --timescope T "
            "--premature P --limit L [--testcasefilter 'TC1 TC2 ...'] | \\\n"
            "  --cmd namemappings | \\\n"
            "  --cmd testcaseswithchanges")

        if http_get:
            printErrorAsJSON("usage error")
        else:
            print error

    # Check for mandatory 'db' argument:
    if "db" in options:
        setDatabase(
            options["dbhost"] if "dbhost" in options else None,
            options["dbport"] if "dbport" in options else None,
            options["db"])
    else:
        printUsageError()
        sys.exit(1)

    # Check for mandatory 'cmd' argument:
    if "cmd" in options:
        cmd = options["cmd"]
    else:
        printUsageError()
        sys.exit(1)

    # Extract the test case filter needed by some commands:
    test_case_filter = (
        tuple(options["testcasefilter"].split())
        if "testcasefilter" in options else None)

    # Return the command if possible:

    # --- 'contexts' ---------------------------------
    if cmd == "contexts":
        return ListContextsAsJSON()

    # --- 'testcases1' ---------------------------------
    elif cmd == "testcases1":
        if ("host" in options and "platform" in options and
            "branch" in options and "sha1" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            sha1 = options["sha1"]
            return ListTestCases1AsJSON(host, platform, branch, sha1)

    # --- 'testcases2' ---------------------------------
    elif cmd == "testcases2":
        if ("host1" in options and "platform1" in options and
            "branch1" in options and "sha11" in options and
            "host2" in options and "platform2" in options and
            "branch2" in options and "sha12" in options):
            host1 = options["host1"]
            platform1 = options["platform1"]
            branch1 = options["branch1"]
            sha11 = options["sha11"]
            host2 = options["host2"]
            platform2 = options["platform2"]
            branch2 = options["branch2"]
            sha12 = options["sha12"]
            return ListTestCases2AsJSON(
                host1, platform1, branch1, sha11, host2, platform2,
                branch2, sha12)

    # --- 'stats1' ---------------------------------
    elif cmd == "stats1":
        if ("host" in options and "platform" in options and
            "branch" in options and "sha1" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            sha1 = options["sha1"]
            return GetStats1AsJSON(
                host, platform, branch, sha1, test_case_filter)

    # --- 'stats2' ---------------------------------
    elif cmd == "stats2":
        if ("host1" in options and "platform1" in options and
            "branch1" in options and "sha11" in options and
            "host2" in options and "platform2" in options and
            "branch2" in options and "sha12" in options):
            host1 = options["host1"]
            platform1 = options["platform1"]
            branch1 = options["branch1"]
            sha11 = options["sha11"]
            host2 = options["host2"]
            platform2 = options["platform2"]
            branch2 = options["branch2"]
            sha12 = options["sha12"]
            return GetStats2AsJSON(
                host1, platform1, branch1, sha11, host2, platform2, branch2,
                sha12, test_case_filter)

    # --- 'result_details2' ---------------------------------
    elif cmd == "result_details2":
        if ("host1" in options and "platform1" in options and
            "branch1" in options and "sha11" in options and
            "host2" in options and "platform2" in options and
            "branch2" in options and "sha12" in options and
            "benchmark" in options and "metric" in options):
            host1 = options["host1"]
            platform1 = options["platform1"]
            branch1 = options["branch1"]
            sha11 = options["sha11"]
            host2 = options["host2"]
            platform2 = options["platform2"]
            branch2 = options["branch2"]
            sha12 = options["sha12"]
            benchmark = options["benchmark"]
            metric = options["metric"]
            return GetResultDetails2AsJSON(
                host1, platform1, branch1, sha11, host2, platform2, branch2,
                sha12, benchmark, metric)

    # --- 'timeseriesstats' ---------------------------------
    elif cmd == "timeseriesstats":
        if ("host" in options and "platform" in options and
            "branch" in options and "sha11" in options and "sha12" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            sha11 = options["sha11"]
            sha12 = options["sha12"]

            if "difftol" in options:
                try:
                    difftol = float(options["difftol"])
                    assert difftol >= 1.0
                except:
                    raise BaseException(
                        "'difftol' not a real value >= 1.0: " +
                        str(options["difftol"]))
            else:
                difftol = 1.1 # 10%

            if "durtolmin" in options:
                try:
                    durtolmin = int(options["durtolmin"])
                    assert durtolmin >= 1
                except:
                    raise BaseException(
                        "'durtolmin' not a positive integer: " +
                        str(options["durtolmin"]))
            else:
                durtolmin = 3

            if "durtolmax" in options:
                try:
                    durtolmax = int(options["durtolmax"])
                    assert durtolmax >= durtolmin
                except:
                    raise BaseException(
                        "'durtolmax' not >= " + durtolmin + ": " +
                        str(options["durtolmax"]))
            else:
                durtolmax = 50

            return GetTimeSeriesStatsAsJSON(
                host, platform, branch, sha11, sha12, difftol, durtolmin,
                durtolmax, test_case_filter)

    # --- 'timeseriesdetails' ---------------------------------
    elif cmd == "timeseriesdetails":
        if ("host" in options and "platform" in options and
            "branch" in options and "sha11" in options and
            "sha12" in options and "difftol" in options and
            "durtolmin" in options and "durtolmax" in options and
            "benchmark" in options and "metric" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            sha11 = options["sha11"]
            sha12 = options["sha12"]
            difftol = float(options["difftol"])
            durtolmin = float(options["durtolmin"])
            durtolmax = float(options["durtolmax"])
            benchmark = options["benchmark"]
            metric = options["metric"]

            return GetTimeSeriesDetailsAsJSON(
                host, platform, branch, sha11, sha12, difftol, durtolmin,
                durtolmax, benchmark, metric)

    # --- 'snapshots' ---------------------------------
    elif cmd == "snapshots":
        if ("host" in options and "platform" in options and
            "branch" in options and "sha11" in options and
            "sha12" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            sha11 = options["sha11"]
            sha12 = options["sha12"]

            return GetSnapshotsAsJSON(host, platform, branch, sha11, sha12)

    # --- 'settimeseriesnote' ---------------------------------
    # ### Hm ... this command doesn't really get statistics, so maybe
    # rename getstats.py to something more generic
    # (like bmclient.py) ....... 2 B DONE!
    elif cmd == "settimeseriesnote":
        if ("host" in options and "platform" in options and
            "branch" in options and "benchmark" in options and
            "metric" in options and "note" in options):
            host = options["host"]
            platform = options["platform"]
            branch = options["branch"]
            benchmark = options["benchmark"]
            metric = options["metric"]
            note = options["note"]

            return SetTimeSeriesNote(
                host, platform, branch, benchmark, metric, note)

    # --- 'topchanges' ---------------------------------
    elif cmd == "topchanges":
        if ("regressions" in options and "last" in options and
            "timescope" in options and "premature" in options and
            "limit" in options):
            regressions = boolOption(options, "regressions")
            last = boolOption(options, "last")
            timescope = int(options["timescope"])
            premature = boolOption(options, "premature")
            limit = int(options["limit"])

            return GetTopChangesAsJSON(
                test_case_filter, regressions, last, timescope, premature,
                limit)

    # --- 'namemappings' ---------------------------------
    elif cmd == "namemappings":
        return GetNameMappingsAsJSON()

    # --- 'testcaseswithchanges' ---------------------------------
    elif cmd == "testcaseswithchanges":
        return GetTestCasesWithChangesAsJSON()


    # No match:
    printUsageError()
    sys.exit(1)

# --- END Global functions ----------------------------------------------


# --- BEGIN Main program ----------------------------------------------

options, http_get = getOptions()
command = createCommand(options, http_get)
command.execute()

# --- END Main program ----------------------------------------------
