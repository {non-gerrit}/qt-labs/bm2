import sys
import json

from dbaccess import execQuery
from misc import textToId, printJSONHeader, getContext

class GetResultDetails2:

    def __init__(
        self, host1, platform1, branch1, sha11, host2, platform2, branch2,
        sha12, benchmark, metric):
        self.host1 = host1
        self.platform1 = platform1
        self.branch1 = branch1
        self.sha11 = sha11
        self.host2 = host2
        self.platform2 = platform2
        self.branch2 = branch2
        self.sha12 = sha12
        self.benchmark = benchmark
        self.metric = metric

        self.host1_id = textToId("host", host1)
        self.platform1_id = textToId("platform", platform1)
        self.branch1_id = textToId("branch", branch1)
        self.sha11_id = textToId("sha1", sha11)
        self.host2_id = textToId("host", host2)
        self.platform2_id = textToId("platform", platform2)
        self.branch2_id = textToId("branch", branch2)
        self.sha12_id = textToId("sha1", sha12)
        self.benchmark_id = textToId("benchmark", benchmark)
        self.metric_id = textToId("metric", metric)

    def execute(self):
        self.sample1 = self.fetchSample(self.host1_id, self.platform1_id,
                self.branch1_id, self.sha11_id, self.benchmark_id,
                self.metric_id)
        self.sample2 = self.fetchSample(self.host2_id, self.platform2_id,
                self.branch2_id, self.sha12_id, self.benchmark_id,
                self.metric_id)
        self.writeOutput()

    # Fetches the sample (i.e. individual observations) for both contexts.
    @classmethod
    def fetchSample(cls, host, platform, branch, sha1, benchmark, metric):
        context = getContext(host, platform, branch, sha1)

        sample = []
        for value, valid, timestamp in execQuery(
                "SELECT value, valid, EXTRACT(EPOCH FROM startTime)::INT"
                " FROM result, upload"
                " WHERE uploadId = upload.id"
                " AND contextId = %s"
                " AND benchmarkId = %s"
                " AND metricId = %s"
                " ORDER BY value DESC",
                (context, benchmark, metric)):
            sample.append({
                    'value' : value,
                    'valid' : valid,
                    'timestamp' : timestamp
                })
        return sample

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'sample1' : self.sample1, 'sample2' : self.sample2 }, sys.stdout)

class GetResultDetails2AsJSON(GetResultDetails2):
    def writeOutput(self):
        self.writeOutputAsJSON()
