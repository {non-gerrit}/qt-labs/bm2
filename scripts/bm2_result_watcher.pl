#!/usr/bin/perl -w
#
# mydaemon.pl by Andrew Ault, www.andrewault.net
#
# Free software. Use this as you wish.
#
# Throughout this template "mydaemon" is used where the name of your daemon should
# be, replace occurrences of "mydaemon" with the name of your daemon.
#
# This name will also be the exact name to give this file (WITHOUT a ".pl" extension).
#
# It is also the exact name to give the start-stop script that will go into the
# /etc/init.d/ directory.
#
# It is also the name of the log file in the /var/log/ directory WITH a ".log"
# file extension.
#
# Replace "# do something" with your super useful code.
#
# Use "# logEntry("log something");" to log whatever your need to see in the log.
#
use strict;
use warnings;
use POSIX;
use File::Pid;
 
my $daemonName    = "bm2resultwatcher";
#
my $dieNow        = 0;                                     # used for "infinte loop" construct - allows daemon mode to gracefully exit
my $sleepMainLoop = 120;                                   # number of seconds to wait between "do something" execution after queue is clear
my $logging       = 1;                                     # 1= logging is on
my $logFilePath   = "/var/log/";                           # log file path
my $logFile       = $logFilePath . $daemonName . ".log";
my $pidFilePath   = "/var/run/";                           # PID file path
my $pidFile       = $pidFilePath . $daemonName . ".pid";
my $workdir       = "/home/qt/benchmarks/";
my $tmpdir        = "/home/qt/tmp_benchmarks/";
 
# daemonize
use POSIX qw(setsid);
chdir '/';
umask 0;
open STDIN,  '/dev/null'   or die logEntry("Can't read /dev/null: $!");
open STDOUT, '>>' . $tmpdir . 'stdout.log' or die logEntry("Can't write to /dev/null: $!");
open STDERR, '>>' . $tmpdir . 'stderr.log' or die logEntry("Can't write to /dev/null: $!");
defined( my $pid = fork ) or die "Can't fork: $!";
exit if $pid;

# turn on logging
if ($logging) {
  open LOG, ">>$logFile";
  select((select(LOG), $|=1)[0]); # make the log file "hot" - turn off buffering
}

# dissociate this process from the controlling terminal that started it and stop being part
# of whatever process group this process was a part of.
POSIX::setsid() or die logEntry("Can't start a new session.");
 
# callback signal handler for signals.
$SIG{INT} = $SIG{TERM} = $SIG{HUP} = \&signalHandler;
$SIG{PIPE} = 'ignore';
 
# create pid file in /var/run/
my $pidfile = File::Pid->new( { file => $pidFile, } );
 
$pidfile->write or die logEntry("Can't write PID file, /dev/null: $!");
 
# "infinite" loop where some useful process happens
until ($dieNow) {
    sleep($sleepMainLoop);
 
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);
    my $timestamp = mktime($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,-1);
    my $hostname;
    my $platform;
    my $branch;
    my $sha1;

    opendir(D, $workdir) || die logEntry("Can't opedir: $!\n");
    while (my $f = readdir(D)) {
      if ($f =~ /xml/ ) {
        open(FILE, $workdir . $f);
        while (my $fi = <FILE>) {
          if ($fi =~ /<HostName>(.*)<\/HostName>/)         { $hostname = $1; next; }
          if ($fi =~ /<MakeSpec>(.*)<\/MakeSpec>/)         { $platform = $1; next; }
          if ($fi =~ /<ChangeNumber>(.*)<\/ChangeNumber>/) { $sha1     = $1; next; }
          if ($fi =~ /<Branch>(.*)<\/Branch>/)             { $branch   = $1; last; }
        }
        close(FILE);
        my $upload_exe = "/home/qt/bm2/scripts/uploadresults.py --db bm --host $hostname --platform $platform --branch $branch --sha1 $sha1 --file $workdir$f";
        logEntry("Trying to parse: uploadresults.py --db bm --host $hostname --platform $platform --branch $branch --sha1 $sha1 --file $workdir$f");
        eval { system($upload_exe) };
        if ($@) {
          logEntry("Couldn't execute: uploadresults.py --db bm --host $hostname --platform $platform --branch $branch --sha1 $sha1 --file $workdir$f");
          logEntry("Couldn't execute: $@");
        } else {
          system("/bin/cp $workdir$f $tmpdir") == 0 or logEntry("Couldn't execute `cp': " . $workdir . $f);
          system("/bin/gzip $tmpdir$f") == 0 or logEntry("Couldn't execute `gzip': " . $tmpdir . $f);
          system("/bin/rm -f $workdir$f") == 0 or logEntry("Couldn't execute `rm': " . $workdir . $f);
          logEntry("Done with result file: uploadresults.py --db bm --host $hostname --platform $platform --branch $branch --sha1 $sha1 --file $workdir$f");
        }
      }
    }
    closedir(D);
}
 
# add a line to the log file
sub logEntry {
    my ($logText) = @_;
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);
    my $dateTime = sprintf "%4d-%02d-%02d %02d:%02d:%02d", $year + 1900, $mon + 1, $mday, $hour, $min, $sec;
    if ($logging) {
        print LOG "$dateTime $logText\n";
    }
}
 
# catch signals and end the program if one is caught.
sub signalHandler {
    $dieNow = 1;    # this will cause the "infinite loop" to exit
}
 
# do this stuff when exit() is called.
END {
    logEntry("Exiting\n");
    if ($logging) { close LOG }
    $pidfile->remove if defined $pidfile;
}
