#!/usr/bin/env python

"""
This script is intended to be executed whenever a new snapshot is complete
for a host/platform/branch combination.

The script registers - in the 'change' table - all current changes of all
time series of the host/platform/branch combination in question (wiping all
existing changes first).

The 'Top Changes' web page can then populate its internal tables
directly from the 'change' table.
"""


import sys
from dbaccess import setDatabase, execQuery, commit
from misc import getOptions, textToId, getAllChanges


# --- BEGIN Global functions ----------------------------------------------

def printUsage():
    sys.stderr.write(
        "usage: " + sys.argv[0] +
        " --help | [--dbhost H] [--dbport P] --db D --host H --platform P "
        "--branch B [--noprogress NP]\n")

def printVerboseUsage():
    printUsage()
    sys.stderr.write("\noptions:\n")
    sys.stderr.write(
        "    --help: This help.\n")
    sys.stderr.write(
        "  --dbhost: The database server host (overriding the default).\n")
    sys.stderr.write(
        "  --dbport: The database server port (overriding the default).\n")
    sys.stderr.write(
        "      --db: The database. One of 'bm' or 'bm-dev' (the latter "
        "intended for experimentation).\n")
    sys.stderr.write(
        "    --host: The physical machine on which the results were "
        "produced (e.g. barbarella or 172.24.90.79).\n")
    sys.stderr.write(
        "--platform: The OS/compiler/architecture combination "
        "(e.g. linux-g++-32).\n")
    sys.stderr.write(
        "  --branch: The product branch (e.g. 'qt 4.6', 'qt 4.7', or "
        "'qt master').\n")
    sys.stderr.write(
        "  --noprogress: Specify \'true\' to disable progress indicator.\n")


# ### 2 B DOCUMENTED!
def printProgress(p, lead):
    sys.stdout.write(lead + " ... (" + "{0:.2f}".format(p) + " %)\r")
    sys.stdout.flush()


# ### 2 B DOCUMENTED!
# NOTE: This function is currently duplicated elsewhere in JavaScipt!
def changeMagnitudeScore(change):
    max_change = 2.0
    abs_change = (1.0 / change) if change < 1 else change
    return (min(abs_change, max_change) - 1.0) / (max_change - 1.0)


# Updates the 'changes' table in the current database with all changes in
# all time series (i.e. benchmark/metric combinations) of the given
# host/platform/branch combination. Progress will be written standard output
# iff no_progress is False.
#
# The algorithm is effectively this:
# - Compute the current changes.
# - Delete all rows matching the given host/platform/branch combination.
# - Add a row for each individual change.
#
def updateChanges(host_id, platform_id, branch_id, no_progress):

    # Hardcode tolerances here for now:
    difftol = 1.1
    durtolmin = 3
    durtolmax = 10

    # Get all changes for this host/platform/branch combination:
    if no_progress:
        sys.stdout.write(
            "getting all changes for this host/platform/branch "
            "combination ... ")
    changes = getAllChanges(
        host_id, platform_id, branch_id, difftol, durtolmin, durtolmax,
        None if no_progress else printProgress,
        "getting all changes for this host/platform/branch combination")

    if no_progress:
        sys.stdout.write("done")
    sys.stdout.write(
        "\n" + str(len(changes)) + " time series with changes found\n")

    if len(changes) == 0:
        sys.stderr.write("error: no time series with changes found\n")
        sys.exit(1)


    # Store the changes in the 'change' table:
    sys.stdout.write("storing to 'change' table ...")
    sys.stdout.flush()

    # ... delete all rows matching this host/platform/branch combination:
    execQuery(
        "DELETE FROM change"
        " WHERE hostId = %s"
        "   AND platformId = %s"
        "   AND branchId = %s", (host_id, platform_id, branch_id), False)

    # ... insert rows for the new changes:
    query = (
        "INSERT INTO change"
        " (benchmarkId, testCaseId, metricId, hostId, platformId, branchId,"
        " sha1Id, timestamp, regression, score, premature_score) VALUES ")
    args = []

    for benchmark_id, metric_id, tschanges in changes:

        test_case_id = execQuery(
             "SELECT testCaseId FROM benchmark WHERE id = %s",
             (benchmark_id,))[0][0];

        for change in tschanges:

            ratio      = change[2]
            regression = ratio < 1.0
            magn_score = changeMagnitudeScore(ratio)
            gsep_score = change[3]
            lsep_score = change[4]
            dur_score1 = change[5]
            dur_score2 = change[6]
            premature_score = magn_score * gsep_score * lsep_score * dur_score1
            score = premature_score * dur_score2
            sha1_id = change[7]
            timestamp = change[8]

            if len(args) > 0:
                query += ", "
            query += ("(%s" + ", %s"*10 + ") ")
            args += (
                benchmark_id, test_case_id, metric_id, host_id, platform_id,
                branch_id, sha1_id, timestamp, regression, score,
                premature_score)

    execQuery(query, args, False)

    sys.stdout.write("done\n")
    sys.stdout.flush()



# --- END Global functions ----------------------------------------------


# --- BEGIN Main program ----------------------------------------------

options, http_get = getOptions()

if "help" in options:
    printVerboseUsage()
    sys.exit(1)

if (not ("db" in options and "host" in options and "platform" in options and
    "branch" in options)):
    printUsage()
    sys.exit(1)

setDatabase(
    options["dbhost"] if "dbhost" in options else None,
    options["dbport"] if "dbport" in options else None,
    options["db"])

host_id = textToId("host", options["host"])
if host_id == -1:
    sys.stderr.write("error: no such host: " + options["host"] + "\n")
    sys.exit(1)
platform_id = textToId("platform", options["platform"])
if platform_id == -1:
    sys.stderr.write("error: no such platform: " + options["platform"] + "\n")
    sys.exit(1)
branch_id = textToId("branch", options["branch"])
if branch_id == -1:
    sys.stderr.write("error: no such branch:" + options["branch"] + "\n")
    sys.exit(1)

updateChanges(
    host_id, platform_id, branch_id,
    ("noprogress" in options) and (
        (options["noprogress"] == "1")
        or (options["noprogress"].lower() == "true")))

# Write to database:
commit()

# --- END Main program ----------------------------------------------
