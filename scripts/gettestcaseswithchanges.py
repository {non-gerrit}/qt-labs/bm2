import sys, json
from dbaccess import execQuery
from misc import printJSONHeader


class GetTestCasesWithChanges:

    def __init__(self):
        pass

    def execute(self):
        self.test_cases = execQuery(
            "SELECT value FROM (SELECT DISTINCT testCaseId FROM change)"
            " AS foo, testCase WHERE testCase.id = testCaseId"
            " ORDER BY value", ())

        # Flatten one level:
        self.test_cases = (
            [item for sublist in self.test_cases for item in sublist])

        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'testCases': self.test_cases }, sys.stdout)


class GetTestCasesWithChangesAsJSON(GetTestCasesWithChanges):
    def writeOutput(self):
        self.writeOutputAsJSON()
