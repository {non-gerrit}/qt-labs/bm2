import sys
import psycopg2

def setDatabase(dbhost_, dbport_, db_):
    global dbhost, dbport, db
    dbhost = dbhost_
    dbport = dbport_
    db = db_
    assert "dbhost" in globals()
    assert "dbport" in globals()
    assert "db" in globals()

def databaseHost():
    global dbhost
    assert "dbhost" in globals()
    return dbhost

def databasePort():
    global dbport
    assert "dbport" in globals()
    return dbport

def database():
    global db
    assert "db" in globals()
    return db

# Connects to the database and creates a global cursor:
def connectDatabase():
    assert "dbhost" in globals()
    assert "dbport" in globals()
    assert "db" in globals()

    default_dbhost = "bmc.test.qt.nokia.com" # Default unless overridden
    default_dbport = 5432 # Default unless overridden
    assert db != None # Needs to be explicitly specified for now
    try:
        conn = psycopg2.connect(
            host=(dbhost if dbhost else default_dbhost),
            port=(dbport if dbport else default_dbport),
            database=db,
            user="bmuser") # Hardcoded for now
    except:
        print "failed to connect to the database:", sys.exc_info()
        sys.exit(1)

    global cursor
    cursor = conn.cursor()


# Executes a query against the database. args contains the arguments to be
# passed to the query. Returns any result set iff fetch_results is true.
def execQuery(query, args, fetch_results = True):
    if not "cursor" in globals():
        connectDatabase()
    assert "cursor" in globals()

    try:
        cursor.execute(query, args)
        if fetch_results:
            return cursor.fetchall()
    except psycopg2.Error:
        print "query failed: >" + query + "<"
        print "reason:", str(sys.exc_info())
        sys.exit(1)


# Commits everything that has been written to the database.
def commit():
    if not "cursor" in globals():
        return
    cursor.connection.commit()
