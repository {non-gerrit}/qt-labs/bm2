import sys
import json

from misc import idToText, textToId, getSnapshots, printJSONHeader

class GetSnapshots:

    def __init__(
        self, host, platform, branch, sha11, sha12):
        self.host = host
        self.platform = platform
        self.branch = branch
        self.sha11 = sha11
        self.sha12 = sha12
        self.host_id = textToId("host", host)
        self.platform_id = textToId("platform", platform)
        self.branch_id = textToId("branch", branch)
        self.sha11_id = textToId("sha1", sha11)
        self.sha12_id = textToId("sha1", sha12)

    def execute(self):
        self.snapshots = []
        for sha1_id, timestamp in getSnapshots(self.host_id, self.platform_id,
                self.branch_id, self.sha11_id, self.sha12_id):
            self.snapshots.append((idToText('sha1', sha1_id), timestamp))

        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'snapshots': self.snapshots }, sys.stdout)

class GetSnapshotsAsJSON(GetSnapshots):
    def writeOutput(self):
        self.writeOutputAsJSON()
