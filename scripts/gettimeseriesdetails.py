import sys
import json
from dbaccess import database
from misc import (
    idToText, textToId,  metricIdToLowerIsBetter, benchmarkToComponents,
    getSnapshots, getTimeSeries, getChanges, getTimeSeriesMiscStats,
    printJSONHeader)

class GetTimeSeriesDetails:

    def __init__(
        self, host, platform, branch, sha11, sha12, difftol, durtolmin,
        durtolmax, benchmark, metric):
        self.host = host
        self.platform = platform
        self.branch = branch
        self.sha11 = sha11
        self.sha12 = sha12
        self.difftol = difftol
        self.durtolmin = durtolmin
        self.durtolmax = durtolmax
        self.benchmark = benchmark
        self.metric = metric
        self.host_id = textToId("host", host)
        self.platform_id = textToId("platform", platform)
        self.branch_id = textToId("branch", branch)
        self.sha11_id = textToId("sha1", sha11)
        self.sha12_id = textToId("sha1", sha12)
        self.benchmark_id = textToId("benchmark", benchmark)
        self.metric_id = textToId("metric", metric)

    def execute(self):
        snapshots = getSnapshots(
            self.host_id, self.platform_id, self.branch_id, self.sha11_id,
            self.sha12_id)

        # Get the content and some basic stats of the time series:
        (self.time_series, self.tot_ninvalid, self.tot_nzeros,
         self.median_of_rses, self.rse_of_medians, self.ms,
         self.lsd) = getTimeSeries(
            self.host_id, self.platform_id, self.branch_id, snapshots,
            self.benchmark_id, self.metric_id)

        # Extract the significant changes:
        changes_all = getChanges(
            self.time_series, metricIdToLowerIsBetter(self.metric_id),
            self.difftol, self.durtolmin, self.durtolmax)
        self.changes = tuple([item[1:3] for item in changes_all])

        self.lib = "1" if metricIdToLowerIsBetter(self.metric_id) else "0"

        # Get misc. statistics:q
        self.stats = {}
        getTimeSeriesMiscStats(
            self.time_series, changes_all, snapshots, self.stats)

        self.writeOutput()

    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({
                'time_series': self.time_series,
                'changes': self.changes,
                'lib': self.lib,
                'ms': self.ms,
                'lsd': self.lsd,
                'ni': self.tot_ninvalid,
                'nz': self.tot_nzeros,
                'nc': len(self.changes),
                'med_of_rses': self.median_of_rses,
                'rse_of_meds': self.rse_of_medians,
                'lc': self.stats["lc"],
                'lc_timestamp': self.stats["lc_timestamp"],
                'lc_distance': self.stats["lc_distance"],
                'lc_gsep_score': self.stats["lc_gsep_score"],
                'lc_lsep_score': self.stats["lc_lsep_score"],
                'lc_dur1_score': self.stats["lc_dur1_score"],
                'lc_dur2_score': self.stats["lc_dur2_score"]
                }, sys.stdout)


class GetTimeSeriesDetailsAsJSON(GetTimeSeriesDetails):
    def writeOutput(self):
        self.writeOutputAsJSON()
