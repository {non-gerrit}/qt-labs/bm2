import sys
import json

from dbaccess import execQuery
from misc import idToText, printJSONHeader

class ListContexts:

    def __init__(self):
        pass


    def execute(self):

        # Get all host/platform/branch/sha1 combinations:
        contexts = execQuery(
                "SELECT id, hostId, platformId, branchId, sha1Id,"
                " EXTRACT(EPOCH FROM timestamp)::INT"
                " FROM context"
                " ORDER BY hostId, platformId,"
                " branchId, timestamp DESC;", ()) \
                + [(-1, -1, -1, -1, -1, -1)] # Sentinel value

        self.contexts = []

        if not len(contexts):
            return

        id, host, platform, branch, sha1, timestamp = contexts[0]

        curHost, curPlatform, curBranch = host, platform, branch
        curSnapshots = [(idToText('sha1', sha1), timestamp)]

        for (id, host, platform, branch, sha1, timestamp) in contexts[1:]:
            if (host, platform, branch) != (curHost, curPlatform, curBranch):
                self.contexts.append({
                        'host' :        idToText('host', curHost),
                        'platform' :    idToText('platform', curPlatform),
                        'branch' :      idToText('branch', curBranch),
                        'snapshots' :   curSnapshots
                        })
                curHost, curPlatform, curBranch = host, platform, branch
                curSnapshots = []

            curSnapshots.append((idToText('sha1', sha1), timestamp))

        self.writeOutput()


    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({ 'contexts' : self.contexts }, sys.stdout)


class ListContextsAsJSON(ListContexts):
    def writeOutput(self):
        self.writeOutputAsJSON()
