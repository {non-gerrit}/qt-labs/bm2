import sys
import json
from dbaccess import execQuery, database
from singlecontextbmstats import (
    SingleContextBMStats, extractSingleContextBMStats)
from misc import (
    idToText, textToId, benchmarkToComponents, printJSONHeader, getContext)

class GetStats1:

    def __init__(self, host, platform, branch, sha1, test_case_filter):
        self.host = host
        self.platform = platform
        self.branch = branch
        self.sha1 = sha1
        self.test_case_filter = test_case_filter
        self.host_id = textToId("host", host)
        self.platform_id = textToId("platform", platform)
        self.branch_id = textToId("branch", branch)
        self.sha1_id = textToId("sha1", sha1)

    def execute(self):
        # self.overall_stats = self.computeOverallStats()
        bmstats_list = self.computeBMStatsList()

        self.per_bm_stats = []
        for stats in bmstats_list:

            s_min = s_max = s_median = s_mean = s_stddev = s_rsd = s_rse = ""

            if stats.nvalid > 0:
                s_min = str(stats.min_)
                s_max = str(stats.max_)
                s_median = str(stats.median)
                s_mean = str(stats.mean)
                if stats.nvalid > 1:
                    s_stddev = str(stats.stddev)
                    if not stats.mean_is_zero:
                        s_rsd = "{0:.2f}".format(stats.rsd)
                        s_rse = "{0:.2f}".format(stats.rse)

            s_metric = idToText("metric", stats.metric_id)
            benchmark = idToText("benchmark", stats.benchmark_id)

            binvals = []
            for binval in stats.hist:
                binvals.append("{0:.0f}".format(binval))

            self.per_bm_stats.append(
                [ stats.ntotal,
                  stats.nvalid,
                  s_min,
                  s_max,
                  s_median,
                  s_mean,
                  s_stddev,
                  s_rsd,
                  s_rse ]
                + binvals
                + [ idToText("metric", stats.metric_id),
                    idToText("benchmark", stats.benchmark_id) ]
                )

        self.writeOutput()


    # 2 B DONE:
    # Computes and returns a dictionary of overall statistics.
    # def computeOverallStats(self):
    #     stats = {}
    #     # Distinct test cases:
    #     ......
    #     # Distinct (test case, test function) combinations:
    #     ......
    #     # Distinct (test case, test function, data tag) combinations:
    #     ......
    #     # (test case, test function, data tag) combinations with at least
    #     # one failed value
    #     ......
    #     return stats


    # Computes per-benchmark statistics. Returns an n-tuple of
    # SingleContextBMStats objects.
    def computeBMStatsList(self):

        # Get the context:
        context_id = getContext(
            self.host_id, self.platform_id, self.branch_id, self.sha1_id)

        # Get all distinct benchmark/metric combinations matching the context:
        bmark_metrics = execQuery(
            "SELECT DISTINCT benchmarkId, metricId"
            " FROM result WHERE contextId = %s", (context_id,))

        bmstats_list = []

        # Loop over benchmarks and compute stats:
        for benchmark_id, metric_id in bmark_metrics:

            benchmark = idToText("benchmark", benchmark_id)
            test_case, test_function, data_tag = (
                benchmarkToComponents(benchmark))
            if ((self.test_case_filter != None)
                and (not test_case in self.test_case_filter)):
                continue

            bmstats = extractSingleContextBMStats(
                context_id, benchmark_id, metric_id)

            bmstats_list.append(bmstats)

        return tuple(bmstats_list)


    def writeOutputAsJSON(self):
        printJSONHeader()
        json.dump({
                'database': database(),
                'host': self.host,
                'platform': self.platform,
                'branch': self.branch,
                'sha1': self.sha1,
                'tc_combs': -1,
                'tc_tf_combs': -1,
                'tc_tf_dt_combs': -1,
                'tc_tf_dt_failed': -1,
                'per_bm_stats': self.per_bm_stats
                }, sys.stdout)


class GetStats1AsJSON(GetStats1):
    def writeOutput(self):
        self.writeOutputAsJSON()
