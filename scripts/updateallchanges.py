#!/usr/bin/env python

"""
This script invokes updatechanges.py for all host/platform/branch combinations.
"""

import sys
from subprocess import Popen, PIPE
from dbaccess import setDatabase, execQuery, commit
from misc import getOptions, idToText


# --- BEGIN Global functions ----------------------------------------------

def printUsage():
    sys.stderr.write(
        "usage: " + sys.argv[0] + " --help | [--dbhost H --dbport P] --db D\n")


def printVerboseUsage():
    printUsage()
    sys.stderr.write("\noptions:\n")
    sys.stderr.write(
        "    --help: This help.\n")
    sys.stderr.write(
        "  --dbhost: The database server host (overriding the default).\n")
    sys.stderr.write(
        "  --dbport: The database server port (overriding the default).\n")
    sys.stderr.write(
        "      --db: The database. One of 'bm' or 'bm-dev' (the latter "
        "intended for experimentation).\n")


# Executes the external updatechanges.py script with appropriate arguments.
def execUpdateChanges(host, platform, branch, options):

    cmd = [
        "updatechanges.py", "--db", options["db"], "--host", host,
        "--platform", platform, "--branch", branch, "--noprogress", "true"]
    if "dbhost" in options:
        cmd += ["--dbhost", options["dbhost"]]
    if "dbport" in options:
        cmd += ["--dbport", options["dbport"]]

    sys.stdout.write(
        "\nupdating changes for " + host + " / " + platform + " / " + branch +
        " ...\n")
    sys.stdout.flush()

    p = Popen(cmd, stdout = PIPE, stderr = PIPE)
    stdout, stderr = p.communicate()
    if (p.returncode != 0):
        sys.stdout.write("failed to execute command '" + str(cmd) + "':\n")
        sys.stdout.write("  return code: " + str(p.returncode) + "\n")
        sys.stdout.write("  stdout: >" + stdout.strip() + "<\n")
        sys.stdout.write("  stderr: >" + stderr.strip() + "<\n")
    else:
        sys.stdout.write("updatechanges.py executed successfully:\n")
        sys.stdout.write("  return code: " + str(p.returncode) + "\n")
        sys.stdout.write("  stdout: >" + stdout.strip() + "<\n")
        sys.stdout.write("  stderr: >" + stderr.strip() + "<\n")

# --- END Global functions ----------------------------------------------


# --- BEGIN Main program ----------------------------------------------

options, http_get = getOptions()

if "help" in options:
    printVerboseUsage()
    sys.exit(1)

if not ("db" in options):
    printUsage()
    sys.exit(1)

setDatabase(
    options["dbhost"] if "dbhost" in options else None,
    options["dbport"] if "dbport" in options else None,
    options["db"])

hpb_ids = execQuery(
    "SELECT DISTINCT hostId, platformId, branchId FROM context "
    "ORDER BY hostId, platformId, branchId", ())

for host_id, platform_id, branch_id in hpb_ids:
    execUpdateChanges(
        idToText("host", host_id),
        idToText("platform", platform_id),
        idToText("branch", branch_id),
        options)

sys.exit(0)

# --- END Main program ----------------------------------------------
