function fetchSnapshots(
    database, host, platform, branch, sha11, sha12, sha1Sel, benchmark,
    metric, difftol, durtolmin, durtolmax) {
    updateStatus("fetching snapshots ...", true);

    query = "?db=" + database +
        "&cmd=snapshots" +
        "&host=" + encodeURIComponent(host) +
        "&platform=" + encodeURIComponent(platform) +
        "&branch=" + encodeURIComponent(branch) +
        "&sha11=" + sha11 +
        "&sha12=" + sha12;

    url = "http://" + location.host + "/cgi-bin/getstatswrapper" + query;
    //alert("url: >" + url + "<");

    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",

        success: function(data, textStatus, request) {
            if (request.readyState == 4) {
                if (request.status == 200) {

                    if (data.error != null) {
                        updateStatus(
                            "fetching snapshots ... failed: " +
                                data.error, false);
                        return;
                    }

                    updateStatus("fetching snapshots ... done", false);
                    updateStatus("", false);

                    setSnapshots(data.snapshots);

                    $("#div_tsbm").css("display", "block");

                    // Fetch and plot time series:
                    fetchTimeSeries(
                        database, host, platform, branch, sha11, sha12,
                        sha1Sel, benchmark, metric, difftol, durtolmin,
                        durtolmax, false);
                }
            }
        },

        error: function(request, textStatus, errorThrown) {
            descr = errorThrown;
            if (errorThrown == null) {
                descr = "undefined error - is the server down?";
            }
            updateStatus("fetching snapshots ... error: " + descr, false);
        }

        // complete: function(request, textStatus) {
        //     alert("complete; request.status: " + request.status)
        // }

    });
}

$(document).ready(function() {

    initTablesorter();
    initTSBMBody();

    var args = queryStringArgs();

    database = extractArg(args, "db");
    if (database == "") {
        alert("ERROR: invalid query string (empty database)");
        return;
    }
    host = extractArg(args, "host");
    if (host == "") {
        alert("ERROR: invalid query string (empty host)");
        return;
    }
    platform = extractArg(args, "platform");
    if (platform == "") {
        alert("ERROR: invalid query string (empty platform)");
        return;
    }
    branch = extractArg(args, "branch");
    if (branch == "") {
        alert("ERROR: invalid query string (empty branch)");
        return;
    }
    var sha11 = extractArg(args, "sha11");
    if (sha11 == "") {
        alert("ERROR: invalid query string (empty sha11)");
        return;
    }
    var sha12 = extractArg(args, "sha12");
    if (sha11 == "") {
        alert("ERROR: invalid query string (empty sha12)");
        return;
    }
    var sha1Sel = extractArg(args, "sha1_sel"); // optional
    var benchmark = extractArg(args, "benchmark");
    if (benchmark == "") {
        alert("ERROR: invalid query string (empty benchmark)");
        return;
    }
    var metric = extractArg(args, "metric");
    if (metric == "") {
        alert("ERROR: invalid query string (empty metric)");
        return;
    }
    var difftol = extractArg(args, "difftol");
    if (difftol == "") {
        alert("ERROR: invalid query string (empty difftol)");
        return;
    }
    var durtolmin = extractArg(args, "durtolmin");
    if (durtolmin == "") {
        alert("ERROR: invalid query string (empty durtolmin)");
        return;
    }
    var durtolmax = extractArg(args, "durtolmax");
    if (durtolmax == "") {
        alert("ERROR: invalid query string (empty durtolmax)");
        return;
    }

    $("#div_tsbm_border").css("display", "none");
    $("#div_tsbm").css("display", "none");
    $("#div_perBenchmarkStats").css("display", "none");

    // Show context ...
    $("#main_context_database").text(database);
    $("#main_context_host").text(host);
    $("#main_context_platform").text(platform);
    $("#main_context_branch").text(branch);
    $("#main_context_sha11").text(sha11);
    $("#main_context_sha12").text(sha12);
    $("#main_context_difftol").text(difftol);
    $("#main_context_durtolmin").text(durtolmin);
    $("#main_context_durtolmax").text(durtolmax);

    // Fetch snapshots:
    fetchSnapshots(
        database, host, platform, branch, sha11, sha12, sha1Sel, benchmark,
        metric, difftol, durtolmin, durtolmax);
});
