// --- BEGIN Global variables -----------------------------------

var database = null;

// Context 1:
var host1 = null;
var platform1 = null;
var branch1 = null;
var sha11 = null;

// Context 2:
var host2 = null;
var platform2 = null;
var branch2 = null;
var sha12 = null;

// Plot:
var plot = null; // Median plot.
var overviewPlot = null; // Overview of median plot.
var initRanges = null; // Initial (fully zoomed out) X- and Y ranges for plot.
var currRanges = null;
var benchmarks = null;
var metrics = null;
var lowerIsBetter = null;
var coords = null;
var logCoords = null;
var itemSelected = null;
var selItem_index = null;
var sel_benchmark = null;
var sel_metric = null;
var sel_coordX = null;
var sel_coordX_descr = null;
var sel_coordY = null;
var sel_coordY_descr = null;

// --- END Global variables -------------------------------------


function plot_clearSelection(skipRedrawOverlay) {
    plot.clearHighlightPixel();
    $(sel_benchmark).text("");
    $(sel_metric).text("");
    $(sel_coordX).text("");
    $(sel_coordX_descr).text("");
    $(sel_coordY).text("");
    $(sel_coordY_descr).text("");

    if (itemSelected) {
        clearResultDetails();
    }

    itemSelected = false;

    if ((skipRedrawOverlay == null) || (!skipRedrawOverlay)) {
        plot.triggerRedrawOverlay();
    }
}

function plot_updateHighlightPixel(index) {
    // Update highlighting of selected point:
    var x = logCoords[index][0];
    var y = logCoords[index][1];
    var xlo = currRanges.xaxis.from;
    var xhi = currRanges.xaxis.to;
    var ylo = currRanges.yaxis.from;
    var yhi = currRanges.yaxis.to;
    var xfrac = (x - xlo) / (xhi - xlo);
    var yfrac = 1 - (y - ylo) / (yhi - ylo);
    var px = xfrac * plot.width();
    var py = yfrac * plot.height();
    plot.setHighlightPixel(px, py);
}

function plot_updateSelectionDetails(index) {
    $(sel_benchmark).text(benchmarks[index]);
    $(sel_metric).text(metrics[index]);

    var valX = null;
    var valY = null;
    var factImprov = null;
    var text = null;
    var color = null;

    if (lowerIsBetter[metrics[index]]) {
        valX = coords[index][0];
        valY = coords[index][1];
        factImprov = valY / valX;
    } else {
        valX = coords[index][1];
        valY = coords[index][0];
        factImprov = valX / valY;
    }

    $(sel_coordX).text(valX);
    $(sel_coordY).text(valY);

    if (factImprov > 1) {
        text = "(" + factImprov.toFixed(4) + " X)";
        color = betterColor;
    } else if (factImprov < 1) {
        text = "(" + factImprov.toFixed(4) + " X)";
        color = worseColor;
    } else {
        text = "(equal)";
        color = equalColor;
    }
    $(sel_coordX_descr).text(text);
    $(sel_coordX_descr).css("color", color);
}

function plot_selectResult(index) {
    plot_clearSelection(true);

    plot_updateHighlightPixel(index);

    itemSelected = true;
    selItem_index = index;

    plot_updateSelectionDetails(index);

    fetchResultDetails(benchmarks[index], metrics[index]);

    plot.triggerRedrawOverlay();
}

// Handles clicking a benchmark radio button.
function table_clickBMRadioButton(cb) {
    if (cb.checked) {
        var index = cb.id.match(/bm_cb:(\d+)/)[1];
        plot_selectResult(index); // synchronize plot
    } else {
        plot_clearSelection(); // synchronize plot
    }
}

function table_selectRow(index) {
    table_clearSelection();
    $("#bm_cb\\:" + index).attr("checked", true);
}

function table_clearSelection() {
    $("#pbmTable input").attr("checked", false);
}

// ### REFACTOR: Similar function in tsstats.js! 2 B DONE!
function fetchResultDetails(benchmark, metric)  {
    updateStatus("fetching result details ...", true);

    query = "?db=" + database +
        "&cmd=result_details2" +
        "&host1=" + encodeURIComponent(host1) +
        "&platform1=" + encodeURIComponent(platform1) +
        "&branch1=" + encodeURIComponent(branch1) +
        "&sha11=" + sha11 +
        "&host2=" + encodeURIComponent(host2) +
        "&platform2=" + encodeURIComponent(platform2) +
        "&branch2=" + encodeURIComponent(branch2) +
        "&sha12=" + sha12 +
        "&benchmark=" + encodeURIComponent(benchmark) +
        "&metric=" + encodeURIComponent(metric);

    url = "http://" + location.host + "/cgi-bin/getstatswrapper" + query;
    // alert("url: >" + url + "<");

    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",

        success: function(data, textStatus, request) {
            if (request.readyState == 4) {
                if (request.status == 200) {

                    if (!itemSelected) {
                        return;
                    }

                    if (data.error != null) {
                        updateStatus(
                            "fetching result details ... failed: " +
                                data.error, false);
                        return
                    }

                    updateStatus("fetching result details ... done", false);
                    updateStatus("", false);

                    // Show samples ...
                    samples = { "1": data.sample1, "2": data.sample2 };
                    var currTime = dateToTimestamp(new Date());
                    for (key in samples) {

                        // $("#sample" + key + " option").remove();

                        sample = samples[key];
                        nvalid = 0;
                        for (i = 0; i < sample.length; ++i) {
                            nvalid += sample[i].valid;
                        }
                        medianIndex = Math.floor(nvalid / 2);
                        twoMedianIndexes = ((nvalid % 2) == 0);
                        validIndex = 0;
                        html = "";
                        for (i = 0; i < sample.length; ++i) {
                            value = sample[i].value;
                            valid = sample[i].valid;
                            secsAgo = currTime - sample[i].timestamp;
                            ageColor_ = ageColor(secsAgo);

                            if (!valid) {
                                bgColor = "#faa";
                            } else {
                                if ((validIndex == medianIndex) ||
                                    (twoMedianIndexes
                                     && (validIndex == (medianIndex - 1)))) {
                                    bgColor = "#ff0";
                                } else {
                                    bgColor = "#fff";
                                }
                                validIndex++;
                            }

                            // $("#sample" + key).append(
                            //     "<option value=\"" + value +
                            //         "\" style=\"background-color:" +
                            //         bgColor + "\">" + value + "</option>");

                            html += "<tr>";
                            html += "<td style=\"background-color:" +
                                bgColor + "; text-align:right\">" +
                                value + "</td>";
                            html += "<td style=\"background-color:" +
                                ageColor_ + "; text-align:right\">" +
                                secsToDays(secsAgo) + "</td>";
                            html += "</tr>";
                        }

                        // Update table:
                        sel = "#sample" + key;
                        $(sel + " tr:gt(0)").remove();
                        $(sel + " > tbody:last").append(html);
                        $(sel).trigger("update");
                        $(sel).trigger("appendCache");
                    }
                }
            }
        },

        error: function(request, textStatus, errorThrown) {
            descr = errorThrown;
            if (errorThrown == null) {
                descr = "undefined error - is the server down?";
            }
            updateStatus("fetching result details ... error: " + descr, false);
        }

        // complete: function(request, textStatus) {
        //     alert("complete; request.status: " + request.status)
        // }

    });
}

function clearResultDetails()  {
    $("#sample1 tr:gt(0)").remove();
    $("#sample2 tr:gt(0)").remove();
}

// A Flot plugin to manipulate a highlight rectangle around a pixel.
// ### 2 B DOCUMENTED!
(function ($) {
    var options = {
        highlightPixel: {
            color: "rgba(255, 255, 0, 1.0)",
            radius: 2,
            lineWidth: 2,
        }
    };

    function init(plot) {

        var pixel = { x: -1, y: -1 };

        plot.setHighlightPixel = function setHighlightPixel(x, y) {
            pixel.x = x;
            pixel.y = y;
        }

        plot.clearHighlightPixel = function clearHighlightPixel() {
            pixel.x = -1;
            pixel.y = -1;
        }

        function drawHighlightPixel(plot, ctx) {
            var x = pixel.x;
            var y = pixel.y;

            if ((x == -1) || (y == -1)) {
                return;
            }

            var plotOffset = plot.getPlotOffset();
            var c = plot.getOptions().highlightPixel;
            var r = c.radius;

            ctx.save();
            ctx.translate(plotOffset.left, plotOffset.top);
            ctx.strokeStyle = c.color;
            ctx.lineWidth = c.lineWidth;

            ctx.beginPath();
            ctx.moveTo(x - r, y - r);
            ctx.lineTo(x + r, y - r);
            ctx.lineTo(x + r, y + r);
            ctx.lineTo(x - r, y + r);
            ctx.lineTo(x - r, y - r);
            ctx.stroke();

            ctx.restore();
        }

        plot.hooks.drawOverlay.push(drawHighlightPixel);
    }

    $.plot.plugins.push({
        init: init,
        options: options,
        name: "highlightpixel",
        version: "0.1"
    });
})(jQuery);


// Creates a Flot plot that shows a set of coordinates as small dots for
// the purpose of showing a rough overview of the performance in one
// context versus another. The performance is expressed in terms of the
// median value in each context.
// ### 2 B DOCUMENTED MORE!
function createPlot(
    canvasId, overviewCanvasId, sizeId, benchmarkId,
    metricId, coordXId, coordXDescrId, hoverCoordXId, hoverCoordXDescrId,
    hoverCoordXRightParenId, coordYId, coordYDescrId, hoverCoordYId,
    hoverCoordYDescrId, hoverCoordYRightParenId) {
    if (coords.length == 0)
        return;

    var sel_canvas = "#" + canvasId;
    var sel_overviewCanvas = "#" + overviewCanvasId;
    var sel_size = "#" + sizeId;

    sel_benchmark = "#" + benchmarkId;
    sel_metric = "#" + metricId;

    sel_coordX = "#" + coordXId;
    sel_coordX_descr = "#" + coordXDescrId;
    var sel_hoverCoordX = "#" + hoverCoordXId;
    var sel_hoverCoordX_descr = "#" + hoverCoordXDescrId;
    var sel_hoverCoordX_rightParen = "#" + hoverCoordXRightParenId;

    sel_coordY = "#" + coordYId;
    sel_coordY_descr = "#" + coordYDescrId;
    var sel_hoverCoordY = "#" + hoverCoordYId;
    var sel_hoverCoordY_descr = "#" + hoverCoordYDescrId;
    var sel_hoverCoordY_rightParen = "#" + hoverCoordYRightParenId;

    // Compute logarithmic coordinates:
    logCoords = [];
    var logCoords_xSmaller = []; // X coordinate smaller
    var indexMap_xSmaller = [];
    var logCoords_ySmaller = []; // Y coordinate smaller
    var indexMap_ySmaller = [];
    var logCoords_equal = []; // Less than 10% difference
    var indexMap_equal = [];
    var minLogValX = null;
    var maxLogValX = null;
    var minLogValY = null;
    var maxLogValY = null;
    var equalityTolerance = 0.1; // 10%

    for (i = 0; i < coords.length; ++i) {
        var x = coords[i][0];
        var y = coords[i][1];
        if (x <= 0) {
            alert("Error creating plot: non-positive x coordinate found: " + x);
            return;
        }
        if (y <= 0) {
            alert("Error creating plot: non-positive y coordinate found: " + y);
            return;
        }

        logx = Math.log(x);
        logy = Math.log(y);

        logCoords[logCoords.length] = [logx, logy];

        if ((Math.abs(x - y) / Math.min(x, y)) < equalityTolerance) {
            logCoords_equal[logCoords_equal.length] = [logx, logy];
            indexMap_equal[indexMap_equal.length] = i;
        } else if (x < y) {
            logCoords_xSmaller[logCoords_xSmaller.length] = [logx, logy];
            indexMap_xSmaller[indexMap_xSmaller.length] = i;
        } else {
            logCoords_ySmaller[logCoords_ySmaller.length] = [logx, logy];
            indexMap_ySmaller[indexMap_ySmaller.length] = i;
        }

        if (i == 0) {
            minLogValX = maxLogValX = logx;
            minLogValY = maxLogValY = logy;
        } else {
            minLogValX = Math.min(minLogValX, logx);
            maxLogValX = Math.max(maxLogValX, logx);
            minLogValY = Math.min(minLogValY, logy);
            maxLogValY = Math.max(maxLogValY, logy);
        }
    }

    $(sel_size).text(
        logCoords_xSmaller.length + logCoords_ySmaller.length
            + logCoords_equal.length);

    var minLogVal = Math.min(minLogValX, minLogValY);
    var maxLogVal = Math.max(maxLogValX, maxLogValY);
    var padding = 0.1 * (maxLogVal - minLogVal);
    var minPlotVal = minLogVal - padding;
    var maxPlotVal = maxLogVal + padding;
    initRanges = {
        xaxis: { from: minPlotVal, to: maxPlotVal },
        yaxis: { from: minPlotVal, to: maxPlotVal }
    }
    currRanges = initRanges;

    betterColor = "#0a0";
    betterColor_pale = "#5a5";
    worseColor = "#a00";
    worseColor_pale = "#a55";
    equalColor = "#555";

    betterColor_point = "#0a0";
    worseColor_point = "#f00";
    equalColor_point = "#888";

    pointRadius = 1;

    var options = {
        xaxis: {
            min: minPlotVal,
            max: maxPlotVal
        },
        yaxis: {
            min: minPlotVal,
            max: maxPlotVal
        },
        grid: {
            show: false,
            hoverable: true,
            clickable: true,
            mouseActiveRadius: 3,
            autoHighlight: true,
            // backgroundColor: "#ffffff"
            backgroundColor: null // using image instead
        },
        highlightPixel: {
            color: "#000",
            radius: 3,
            lineWidth: 2
        },
        lineWidth: 0,
        selection: { mode: "xy" }

    };

    plot = $.plot(
        $(sel_canvas),
        [
            {
                color: betterColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: pointRadius,
                    fill: false,
                },
                data: logCoords_xSmaller,
                indexMap: indexMap_xSmaller
            },

            {
                color: worseColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: pointRadius,
                    fill: false,
                },
                data: logCoords_ySmaller,
                indexMap: indexMap_ySmaller
            },

            {
                color: equalColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: pointRadius,
                    fill: false,
                },
                data: logCoords_equal,
                indexMap: indexMap_equal
            },

            // dummy points to force padding:
            {
                hoverable: false,
                clickable: false,
                color: "#aaa", // same as background color
                shadowSize: 0,
                lines: { show: false },
                points: { show: true, radius: 0 },
                data: [
                    [minPlotVal, minPlotVal]
                    ,[maxPlotVal, maxPlotVal]
                ]
            }
        ],
        options
    );

    itemSelected = false;
    selItem_index = null;

    $(sel_canvas).bind("plothover", function (event, pos, item) {
        if (!itemSelected) {
            if (item) {
                plot.highlight(item.series, item.datapoint);
                index = item.series.indexMap[item.dataIndex];
                plot_updateSelectionDetails(index);
            } else {
                plot.unhighlight();
                $(sel_benchmark).text("");
                $(sel_metric).text("");
                $(sel_coordX).text("");
                $(sel_coordX_descr).text("");
                $(sel_coordY).text("");
                $(sel_coordY_descr).text("");
            }
        }
    });

    $(sel_canvas).bind("mouseleave", function (event, pos, item) {
        $(sel_hoverCoordX).text("");
        $(sel_hoverCoordX_descr).text("");
        $(sel_hoverCoordX_rightParen).text("");
        $(sel_hoverCoordY).text("");
        $(sel_hoverCoordY_descr).text("");
        $(sel_hoverCoordY_rightParen).text("");
    });

    $(sel_canvas).bind("plotclick", function (event, pos, item) {

        plot.unhighlight();

        if (item) {
            index = item.series.indexMap[item.dataIndex];
            plot_selectResult(index);
            table_selectRow(index); // synchronize table
        } else {
            plot_clearSelection();
            table_clearSelection(); // synchronize table
        }
    });


    // Set up overview plot:
    overviewPlot = $.plot(
        $(sel_overviewCanvas),
        [
            {
                color: betterColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: 1,
                    fill: false,
                },
                data: logCoords_xSmaller
            },
            {
                color: worseColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: 1,
                    fill: false,
                },
                data: logCoords_ySmaller
            },
            {
                color: equalColor_point,
                lines: { show: false },
                points: {
                    show: true,
                    radius: 1,
                    fill: false,
                },
                data: logCoords_equal
            },
            {
                hoverable: false,
                clickable: false,
                color: "#000",
                shadowSize: 0,
                lines: { show: false },
                points: { show: false },
                data: [
                    [minPlotVal, minPlotVal]
                    ,[maxPlotVal, maxPlotVal]
                ]
            }
        ],
        {
            series: {
                // color: "#00ffff",
                lines: { show: false },
                points: { show: true, radius: 1, fill: false },
                shadowSize: 0
            },
            grid: { show: false, backgroundColor: null },
            selection: { mode: "xy" }
        }
    );

    // Connect the main plot to the overview plot:
    $(sel_canvas).bind("plotselected", function (event, ranges) {

        currRanges = ranges;

        // Clamp the zooming to prevent eternal zoom:
        if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
            ranges.xaxis.to = ranges.xaxis.from + 0.00001;
        if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
            ranges.yaxis.to = ranges.yaxis.from + 0.00001;

        // Do the zooming:
        plot = $.plot(
            $(sel_canvas),
            [
                {
                    color: betterColor_point,
                    lines: { show: false },
                    points: {
                        show: true,
                        radius: pointRadius,
                        fill: false,
                    },
                    data: logCoords_xSmaller,
                    indexMap: indexMap_xSmaller
                },
                {
                    color: worseColor_point,
                    lines: { show: false },
                    points: {
                        show: true,
                        radius: pointRadius,
                        fill: false,
                    },
                    data: logCoords_ySmaller,
                    indexMap: indexMap_ySmaller
                },
                {
                    color: equalColor_point,
                    lines: { show: false },
                    points: {
                        show: true,
                        radius: pointRadius,
                        fill: false,
                    },
                    data: logCoords_equal,
                    indexMap: indexMap_equal
                }
                // ,
                // {
                //     hoverable: false,
                //     clickable: false,
                //     color: "#ff0000",
                //     shadowSize: 0,
                //     lines: { show: true },
                //     points: { show: false },
                //     data: [
                //         [minPlotVal, minPlotVal]
                //         ,[maxPlotVal, maxPlotVal]
                //     ]
                // }
            ],

            $.extend(true, {}, options, {
                xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
            }));

        if (itemSelected) {
            plot_updateHighlightPixel(selItem_index);
            plot.triggerRedrawOverlay();
        }

        // Don't fire event on the overview to prevent eternal loop:
        overviewPlot.setSelection(ranges, true);
    });
    $(sel_overviewCanvas).bind("plotselected", function (event, ranges) {
        plot.setSelection(ranges);
    });
}

function fetchStats(testCaseFilter) {

    updateStatus("fetching statistics ...", true);

    query = "?db=" + database +
        "&cmd=stats2" +
        "&host1=" + encodeURIComponent(host1) +
        "&platform1=" + encodeURIComponent(platform1) +
        "&branch1=" + encodeURIComponent(branch1) +
        "&sha11=" + sha11 +
        "&host2=" + encodeURIComponent(host2) +
        "&platform2=" + encodeURIComponent(platform2) +
        "&branch2=" + encodeURIComponent(branch2) +
        "&sha12=" + sha12;

    if (testCaseFilter != "") {
        query += "&testcasefilter=" + testCaseFilter;
    }

    url = "http://" + location.host + "/cgi-bin/getstatswrapper" + query;
    //alert("url: >" + url + "<");

    function setImprovText(sel, val, suffix) {
        fact = Math.pow(2, val);
        betterColor = "#0a0";
        worseColor = "#a00";
        equalColor = "#555";
        sfx = ((suffix != null) && suffix);
        if (fact > 1) {
            $(sel).text(fact.toFixed(4) + " X" + (sfx ? " better" : ""));
            $(sel).css("color", betterColor);
            $(sel).css("text-align", "right");
        } else if (fact < 1) {
            $(sel).text(fact.toFixed(4) + " X" + (sfx ? " better" : ""));
            $(sel).css("color", worseColor);
            $(sel).css("text-align", "right");
        } else {
            $(sel).text("equal");
            $(sel).css("color", equalColor);
        }
    }

    function getImprovFactColorAndText(log2improvFact) {
        color = "#f00";
        text = "n/a";
        if (log2improvFact != "") {
            improvFact = Math.pow(2, log2improvFact);
            if (improvFact > 1.1) {
                color = "#0a0";
            } else if (improvFact < (1 / 1.1)) {
                color = "#a00";
            } else {
                color = "#000";
            }
            text = improvFact.toFixed(4) + " X";
        }
        return {"color": color, "text": text};
    }

    // Assume -1 <= improvDiff <= 1
    function getImprovDiffColorAndText(improvDiff) {
        color = "#f00";
        text = "n/a";
        if (improvDiff != "") {
            improvDiff = parseFloat(improvDiff);
            if (improvDiff > 0.1) {
                color = "#0a0";
            } else if (improvDiff < -0.1) {
                color = "#a00";
            } else {
                color = "#000";
            }
            text = improvDiff.toFixed(4);
        }
        return {"color": color, "text": text};
    }


    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",

        success: function(data, textStatus, request) {
            if (request.readyState == 4) {
                if (request.status == 200) {

                    if (data.error != null) {
                        updateStatus(
                            "fetching statistics ... failed: " +
                                data.error, false);
                        return
                    }

                    updateStatus("fetching statistics ... done", false);
                    updateStatus("", false);

                    // Show context ...
                    $("#shown_database").text(data.database);
                    $("#shown_host1").text(data.host1);
                    $("#shown_platform1").text(data.platform1);
                    $("#shown_branch1").text(data.branch1);
                    $("#shown_sha11").text(data.sha11);
                    $("#shown_host2").text(data.host2);
                    $("#shown_platform2").text(data.platform2);
                    $("#shown_branch2").text(data.branch2);
                    $("#shown_sha12").text(data.sha12);


                    // --- BEGIN Populate overall stats table ---
                    $("#nbenchmarks").text(data.nbenchmarks);
                    $("#ntestcases").text(data.ntestcases);
                    $("#nmedian_improvs").text(data.nmedian_improvs);

                    setImprovText(
                        "#mean_of_median_improvs", data.mean_of_median_improvs);
                    setImprovText(
                        "#context2_descr", data.mean_of_median_improvs, true);
                    setTooltip(
                        $("#mean_of_median_improvs"),
                        "Arithmetic mean of the median improvements " +
                            "from Context 1 to Context 2.");

                    percValues = {
                        "10": data.perc_10_of_median_improvs,
                        "25": data.perc_25_of_median_improvs,
                        "50": data.perc_50_of_median_improvs,
                        "75": data.perc_75_of_median_improvs,
                        "90": data.perc_90_of_median_improvs
                    };
                    for (key in percValues) {
                        setImprovText(
                            "#perc_" + key + "_of_median_improvs",
                            percValues[key]);
                        setTooltip(
                            $("#perc_" + key + "_of_median_improvs"),
                            "The value below which " + key + "% of the " +
                                "median improvements from Context 1 " +
                                "to Context 2 fall.<br /><br />" +
                                "Context 2 improves by this factor or less " +
                                "for the worst " + key + "% of the " +
                                "benchmarks (and improves by this factor " +
                                "or more for the best " + (100 - key) +
                                "%).");
                    }

                    $("#better_than_fact_1_1_count").text(
                        data.better_than_fact_1_1_count);

                    $("#equal_count").text(
                        parseInt(data.nmedian_improvs) -
                            (parseInt(data.better_than_fact_1_1_count) +
                             parseInt(data.worse_than_fact_inv_1_1_count)));
                    $("#worse_than_fact_inv_1_1_count").text(
                        data.worse_than_fact_inv_1_1_count);

                    // --- END Populate overall stats table ---


                    // --- BEGIN Populate per-benchmark stats table ---

                    // Remove all rows below the header ...
                    $("#pbmTable tr:gt(0)").remove();

                    pbm_stats = data.per_bm_stats;

                    // Show # of per-benchmark rows ...
                    $("#pbmTable_nrows").text(pbm_stats.length);

                    html = "";
                    coords = [];
                    benchmarks = [];
                    metrics = [];
                    lowerIsBetter = new Object();
                    for (var i = 0; i < pbm_stats.length; ++i) {
                        row = pbm_stats[i];

                        median1 = row[0];
                        median2 = row[1];
                        medianImprov = row[2];
                        rse1 = row[3];
                        rse2 = row[4];
                        rseImprov = row[5];
                        metric = row[6];
                        lib = parseInt(row[7]);
                        benchmark = row[8];

                        // Append to plottable array ...
                        x = parseFloat(median2);
                        y = parseFloat(median1);
                        if (!(isNaN(x) || isNaN(y) || (x <= 0) || (y <= 0))) {
                            index = coords.length;
                            if (lib) {
                                coords[index] = [x, y];
                            } else {
                                coords[index] = [y, x];
                            }
                            benchmarks[index] = benchmark;
                            metrics[index] = metric;
                            lowerIsBetter[metric] = lib;

                            firstColHtml = "<input id=\"bm_cb:" + index +
                                "\" name=\"currSelector\" " +
                                "type=\"radio\" onclick=\"" +
                                "table_clickBMRadioButton(this)\">";
                        } else {
                            firstColHtml =
                                "<span style=\"color:red\">n/a</span>";
                        }

                        // Append to HTML ...
                        html += "<tr>";

                        html += "<td>" + firstColHtml + "</td>"

                        html += "<td class=\"context1\">" + median1 + "</td>";
                        html += "<td class=\"context2\">" + median2 + "</td>";
                        ict = getImprovFactColorAndText(medianImprov);
                        html += "<td style=\"color: " + ict.color +
                            "; text-align:right\">" + ict.text + "</td>";

                        html += "<td class=\"context1\" " +
                            "style=\"text-align:right\">" +
                            ((rse1 != "") ? parseFloat(rse1).toFixed(2) : "") +
                            "</td>";
                        html += "<td class=\"context2\" " +
                            "style=\"text-align:right\">" +
                            ((rse2 != "") ? parseFloat(rse2).toFixed(2) : "") +
                            "</td>";
                        ict = getImprovDiffColorAndText(rseImprov);
                        html += "<td style=\"color: " + ict.color +
                            "; text-align:right\">" + ict.text + "</td>";

                        html += "<td class=\"metric\">" + metric + "</td>";
                        html += "<td class=\"benchmark\">" + benchmark +
                            "</td>";
                        html += "</tr>";
                    }

                    $("#pbmTable > tbody:last").append(html);
                    $("#pbmTable").trigger("update");
                    $("#pbmTable").trigger("appendCache");

                    // var sorting = [[11,1],[0,0]];
                    //$("table").trigger("sorton",[sorting]);

                    // --- END Populate per-benchmark stats table ---

                    $("#div_results").css("display", "block");

                    createPlot(
                        "plot_canvas_medians", "plot_overview_canvas_medians",
                        "plot_size", "plot_benchmark", "plot_metric",
                        "plot_median2", "plot_median2_descr",
                        "plot_hoverMedian2", "plot_hoverMedian2_descr",
                        "plot_hoverMedian2_rightParen",
                        "plot_median1", "plot_median1_descr",
                        "plot_hoverMedian1", "plot_hoverMedian1_descr",
                        "plot_hoverMedian1_rightParen");
                }
            }
        },

        error: function(request, textStatus, errorThrown) {
            descr = errorThrown;
            if (errorThrown == null) {
                descr = "undefined error - is the server down?";
            }
            updateStatus("fetching statistics ... error: " + descr, false);
        }

        // complete: function(request, textStatus) {
        //     alert("complete; request.status: " + request.status)
        // }

    });
}

$(document).ready(function() {

    $.tablesorter.addParser({
        id: "mixed_numeric",
        is: function(s) {
            return false; // so this parser is not auto detected
        },
        format: function(s) {
            f = parseFloat(s);
            if (isNaN(f)) { return ""; }
            return f.toFixed(20); // max precision guaranteed by ECMA standard
        },
        type: "numeric"
    });

    $("#pbmTable").tablesorter({
        headers: {
            0: { sorter: false },
            // handle proper sorting of mixed standard- and scientific notation:
            1: { sorter: "mixed_numeric" }, // median 1
            2: { sorter: "mixed_numeric" }, // median 2
            3: { sorter: "mixed_numeric" }, // median improvement factor
            4: { sorter: "mixed_numeric" }, // relative standard error 1
            5: { sorter: "mixed_numeric" }, // relative standard error 2
            6: { sorter: "mixed_numeric" }  // RSE improvement factor
        }
    });

    $("#pbmTable").bind("sortStart",function() {
        $("#pbmTable_sortInProgress").show();
    }).bind("sortEnd",function() {
        $("#pbmTable_sortInProgress").hide();
    });

    $("#sample1").tablesorter({
        headers: {
            0: { sorter: "mixed_numeric" },
            1: { sorter: "mixed_numeric" }
        }
    });

    $("#sample2").tablesorter({
        headers: {
            0: { sorter: "mixed_numeric" },
            1: { sorter: "mixed_numeric" }
        }
    });

    args = queryStringArgs();

    // Extract database (required):
    database = extractArg(args, "db");
    if (database == "") {
        alert("ERROR: invalid query string (empty database)");
        return;
    }

    // Extract context 1 (required):
    host1 = extractArg(args, "host1");
    if (host1 == "") {
        alert("ERROR: invalid query string (empty host)");
        return;
    }
    platform1 = extractArg(args, "platform1");
    if (platform1 == "") {
        alert("ERROR: invalid query string (empty platform)");
        return;
    }
    branch1 = extractArg(args, "branch1");
    if (branch1 == "") {
        alert("ERROR: invalid query string (empty branch)");
        return;
    }
    sha11 = extractArg(args, "sha11");
    if (sha11 == "") {
        alert("ERROR: invalid query string (empty sha1)");
        return;
    }

    // Extract context 2 (optional):
    host2 = extractArg(args, "host2");
    platform2 = extractArg(args, "platform2");
    branch2 = extractArg(args, "branch2");
    sha12 = extractArg(args, "sha12");

    // Extract test case filter (optional);
    var testCaseFilter = extractArg(args, "testcasefilter");

    $("#div_results").css("display", "none");

    fetchStats(testCaseFilter);
});

function zoomOutMedianPlot() {
    plot.setSelection(initRanges);
    overviewPlot.clearSelection(true);
    return false;
}
