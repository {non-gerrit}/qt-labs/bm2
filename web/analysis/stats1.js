function fetchStats(database, host, platform, branch, sha1, testCaseFilter) {
    updateStatus("fetching statistics ...", true);

    query = "?db=" + database +
        "&cmd=stats1" +
        "&host=" + encodeURIComponent(host) +
        "&platform=" + encodeURIComponent(platform) +
        "&branch=" + encodeURIComponent(branch) +
        "&sha1=" + sha1;
    if (testCaseFilter != "") {
        query += "&testcasefilter=" + testCaseFilter;
    }

    url = "http://" + location.host + "/cgi-bin/getstatswrapper" + query;
    // alert("url: >" + url + "<");

    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",

        success: function(data, textStatus, request) {
            if (request.readyState == 4) {
                if (request.status == 200) {

                    if (data.error != null) {
                        updateStatus(
                            "fetching statistics ... failed: " +
                                data.error, false);
                        return
                    }

                    updateStatus("fetching statistics ... done", false);
                    updateStatus("", false);

                    // Show context ...
                    $("#shown_database").text(data.database);
                    $("#shown_host").text(data.host);
                    $("#shown_platform").text(data.platform);
                    $("#shown_branch").text(data.branch);
                    $("#shown_sha1").text(data.sha1);

                    // --- BEGIN Populate overall stats table ---
                    // 2 B DONE!
                    // --- END Populate overall stats table ---


                    // --- BEGIN Populate per-benchmark stats table ---

                    // Remove all rows below the header ...
                    $("#pbmTable tr:gt(0)").remove();

                    pbm_stats = data.per_bm_stats;

                    // Show # of per-benchmark rows ...
                    $("#pbmTable_nrows").text(pbm_stats.length);

                    // Insert new rows ...
                    html = "";
                    for (i = 0; i < pbm_stats.length; ++i) {
                        row = pbm_stats[i];
                        html += "<tr>";
                        for (j = 0; j < row.length; ++j) {

                            if (j > 8 && j < 19) {
                                if (row[j] < 0) {
                                    for (k = 0; k < 10; ++k) {
                                        html +=
                                        "<td style=\"color:#ff0000\">n/a</td>";
                                    }
                                    j = 18;
                                } else {
                                    html +=
                                    "<td " +
                                        "style=\"text-align:right\">" +
                                        row[j] + "</td>";
                                }
                            } else if (j <= 8) {
                                if (row[j] != "") {
                                    html +=
                                    "<td style=\"text-align:right\">" +
                                        row[j] + "</td>";
                                } else {
                                    html += "<td></td>";
                                }
                            } else if (j == 19) {
                                html += "<td class=\"metric\">" + row[j] +
                                    "</td>";
                            } else if (j > 19) {
                                html += "<td class=\"benchmark\">" + row[j] +
                                    "</td>";
                            } else {
                                html += "<td>" + row[j] + "</td>";
                            }
                        }
                        html += "</tr>";
                    }
                    $("#pbmTable > tbody:last").append(html);
                    $("#pbmTable").trigger("update");
                    $("#pbmTable").trigger("appendCache");

                    // var sorting = [[11,1],[0,0]];
                    //$("table").trigger("sorton",[sorting]);

                    // --- END Populate per-benchmark stats table ---


                    $("#div_shownContext").css("display", "block");
                    $("#div_statistics").css("display", "block");
                }
            }
        },

        error: function(request, textStatus, errorThrown) {
            descr = errorThrown;
            if (errorThrown == null) {
                descr = "undefined error - is the server down?";
            }
            updateStatus("fetching statistics ... error: " + descr, false);
        }

        // complete: function(request, textStatus) {
        //     alert("complete; request.status: " + request.status)
        // }

    });
}

$(document).ready(function() {

    $.tablesorter.addParser({
        id: "mixed_numeric",
        is: function(s) {
            return false; // so this parser is not auto detected
        },
        format: function(s) {
            f = parseFloat(s);
            if (isNaN(f)) { return ""; }
            return f.toFixed(20); // max precision guaranteed by ECMA standard
        },
        type: "numeric"
    });

    $("#pbmTable").tablesorter({
        headers: {
            // disable sorting:
            // 9: { sorter: false }, // histogram

            // handle proper sorting of mixed standard- and scientific notation:
            2: { sorter: "mixed_numeric" }, // min
            3: { sorter: "mixed_numeric" }, // max
            4: { sorter: "mixed_numeric" }, // median
            5: { sorter: "mixed_numeric" }, // mean
            6: { sorter: "mixed_numeric" }, // standard deviation
            7: { sorter: "mixed_numeric" }, // relative standard deviation
            8: { sorter: "mixed_numeric" }, // relative standard error
            9: { sorter: "mixed_numeric" }, // relative standard error
            10: { sorter: "mixed_numeric" }, // H0
            11: { sorter: "mixed_numeric" }, // H1
            12: { sorter: "mixed_numeric" }, // H2
            13: { sorter: "mixed_numeric" }, // H3
            14: { sorter: "mixed_numeric" }, // H4
            15: { sorter: "mixed_numeric" }, // H5
            16: { sorter: "mixed_numeric" }, // H6
            17: { sorter: "mixed_numeric" }, // H7
            18: { sorter: "mixed_numeric" }, // H8
            19: { sorter: "mixed_numeric" }  // H9
        }
    });

    $("#pbmTable").bind("sortStart",function() {
        $("#pbmTable_sortInProgress").show();
    }).bind("sortEnd",function() {
        $("#pbmTable_sortInProgress").hide();
    });

    args = queryStringArgs();

    // Extract database (required):
    database = extractArg(args, "db");
    if (database == "") {
        alert("ERROR: invalid query string (empty database)");
        return;
    }

    // Extract context (required):
    host = extractArg(args, "host");
    if (host == "") {
        alert("ERROR: invalid query string (empty host)");
        return;
    }
    platform = extractArg(args, "platform");
    if (platform == "") {
        alert("ERROR: invalid query string (empty platform)");
        return;
    }
    branch = extractArg(args, "branch");
    if (branch == "") {
        alert("ERROR: invalid query string (empty branch)");
        return;
    }
    sha1 = extractArg(args, "sha1");
    if (sha1 == "") {
        alert("ERROR: invalid query string (empty sha1)");
        return;
    }

    // Extract test case filter (optional);
    testCaseFilter = extractArg(args, "testcasefilter");

    $("#div_shownContext").css("display", "none");
    $("#div_statistics").css("display", "none");

    fetchStats(database, host, platform, branch, sha1, testCaseFilter);
});
