// Handles selecting a per-benchmark table.
function selectPbmTable() {
    var val = $("#select_pbmTable").attr("value");
    var types = ["all", "lcRegr", "lcImpr"];
    for (index in types) {
        var type = types[index];
        $("#div_pbmTable_" + type).css(
            "display", (val == type) ? "block" : "none");
    }

    var nrows = $("#pbmTable_" + val).find("tr").length - 1;
    $("#pbmTable_nrows").text(nrows + ((nrows == 1) ? " row" : " rows"));
}

function changeMagnitudeScore(change) {
    var maxChange = 2.0;
    var absChange = (change < 1.0) ? (1.0 / change) : change;
    return (Math.min(absChange, maxChange) - 1.0) / (maxChange - 1.0);
}

function populatePbmTable(tableSel, data) {

    // Remove all rows below the header ...
    $(tableSel + " tr:gt(0)").remove();

    pbm_stats = data.per_bm_stats;

    var currTime = dateToTimestamp(currDate);

    var html = "";
    for (var i = 0; i < pbm_stats.length; ++i) {

        stats = pbm_stats[i];

        var lsd = stats.lsd;
        var nc = null;
        var lc = null;
        if (lsd >= 0) {
            nc = stats.nc;
            if (nc > 0)
                lc = stats.lc;
        }
        if (tableSel == "#pbmTable_lcRegr") {
            if ((lc == null) || (lc >= 1))
                continue;
        } else if (tableSel == "#pbmTable_lcImpr") {
            if ((lc == null) || (lc <= 1))
                continue;
        }

        // NOTE: Unused for now:
        //lowerIsBetter = stats.lib;

        var ms = null;
        var ni = null;
        var nz = null;
        var mdrse = null;
        var rsemd = null;
        var qs = null;
        var lcda = null;
        var lcd = null;
        var lcms = null;
        var lcss = null;
        var lcss1 = null;
        var lcgss = null;
        var lclss = null;
        var lcds1 = null;
        var lcds2 = null;

        var rbdy = "";

        var ms = stats.ms;
        rbdy += "<td>" + ms + "</td>";

        if (lsd >= 0) {
            rbdy += "<td>" + lsd + "</td>";

            ni = stats.ni;
            rbdy += "<td>" + ni + "</td>";

            nz = stats.nz;
            rbdy += "<td>" + nz + "</td>";

            nc = stats.nc;
            rbdy += "<td>" + nc + "</td>";

            mdrse = stats.med_of_rses;
            if (mdrse >= 0) {
                rbdy += "<td style=\"text-align:right\">" +
                    mdrse.toFixed(2) + "</td>";
            } else {
                rbdy += "<td></td>";
            }

            rsemd = stats.rse_of_meds;
            if (rsemd >= 0) {
                rbdy += "<td style=\"text-align:right\">" +
                    rsemd.toFixed(2) + "</td>";
            } else {
                rbdy += "<td></td>";
            }

            qs = qualityScore(lsd, ni, nz, nc, mdrse);
            rbdy += "<td>" + qs.toFixed(4) + "</td>";

            if (nc > 0) {

                rbdy += "<td style=\"text-align:right; color:" +
                    ((lc < 1) ? "#a00" : ((lc > 1) ? "#0a0" : "#000")) +
                    "\">" + lc.toFixed(4) + "</td>";

                var secsAgo = currTime - stats.lc_timestamp;
                lcda = secsToDays(secsAgo);
                lcd = stats.lc_distance;
                rbdy += "<td style=\"background-color:" + ageColor(secsAgo) +
                    "; text-align:right\">" + lcda + "&nbsp;(" + lcd + ")" +
                    "</td>";

                lcms = changeMagnitudeScore(lc);
                rbdy += "<td>" + lcms.toFixed(4) + "</td>";

                lcgss = stats.lc_gsep_score;
                lclss = stats.lc_lsep_score;
                lcds1 = stats.lc_dur1_score;
                lcds2 = stats.lc_dur2_score;

                lcss = lcms * lcgss * lclss * lcds1 * lcds2;
                rbdy += "<td>" + lcss + "</td>";

                lcss1 = lcms * lcgss * lclss * lcds1;
                rbdy += "<td>" + lcss1 + "</td>";

                rbdy += "<td>" + lcgss.toFixed(4) + "</td>";
                rbdy += "<td>" + lclss.toFixed(4) + "</td>";
                rbdy += "<td>" + lcds1.toFixed(4) + "</td>";
                rbdy += "<td>" + lcds2.toFixed(4) + "</td>";

            } else {
                rbdy += "<td></td>"; // LC
                rbdy += "<td></td>"; // LCDA
                rbdy += "<td></td>"; // LCMS
                rbdy += "<td></td>"; // LCSS
                rbdy += "<td></td>"; // LCSS1
                rbdy += "<td></td>"; // LCGSS
                rbdy += "<td></td>"; // LCLSS
                rbdy += "<td></td>"; // LCDS1
                rbdy += "<td></td>"; // LCDS2
            }
        } else {
            rbdy += "<td></td>"; // LSD
            rbdy += "<td></td>"; // NI
            rbdy += "<td></td>"; // NZ
            rbdy += "<td></td>"; // NC
            rbdy += "<td></td>"; // MDRSE
            rbdy += "<td></td>"; // RSEMD
            rbdy += "<td></td>"; // QS
            rbdy += "<td></td>"; // LC
            rbdy += "<td></td>"; // LCDA
            rbdy += "<td></td>"; // LCMS
            rbdy += "<td></td>"; // LCSS
            rbdy += "<td></td>"; // LCSS1
            rbdy += "<td></td>"; // LCGSS
            rbdy += "<td></td>"; // LCLSS
            rbdy += "<td></td>"; // LCDS1
            rbdy += "<td></td>"; // LCDS2
        }

        rbdy = "<td><input name=\"currSelector\" type=\"radio\" onclick=\"" +
            "clickBMRadioButton(this, '" + tableSel + "', '" +
            data.database + "', '" + data.host + "', '" +
            data.platform + "', '" + data.branch + "', '" +
            data.sha11 + "', '" + data.sha12 + "', '" +
            stats.bm + "', '" + stats.mt + "', " +
            data.difftol + ", " + data.durtolmin + ", " +
            data.durtolmax +
            ")\"></td>" +
            rbdy;

        rbdy += "<td class=\"metric\">" + stats.mt + "</td>";
        rbdy += "<td class=\"benchmark\">" +
            stats.bm.replace(anySpace, "&nbsp;") + "</td>";

        html += "<tr>" + rbdy + "</tr>";
    }

    $(tableSel + " > tbody:last").append(html);
    $(tableSel).trigger("update");
    if (html != "") // hm ... why is this test necessary?
        $(tableSel).trigger("appendCache");

    // var sorting = [[11,1],[0,0]];
    //$("table").trigger("sorton",[sorting]);
}

function fetchStats(
    database, host, platform, branch, sha11, sha12, difftol, durtolmin,
    durtolmax, testCaseFilter) {
    updateStatus("fetching statistics ...", true);

    query = "?db=" + database +
        "&cmd=timeseriesstats" +
        "&host=" + encodeURIComponent(host) +
        "&platform=" + encodeURIComponent(platform) +
        "&branch=" + encodeURIComponent(branch) +
        "&sha11=" + sha11 +
        "&sha12=" + sha12;
    if (difftol != "")
        query += "&difftol=" + difftol;
    if (durtolmin != "")
        query += "&durtolmin=" + durtolmin;
    if (durtolmax != "")
        query += "&durtolmax=" + durtolmax;
    if (testCaseFilter != "")
        query += "&testcasefilter=" + encodeURIComponent(testCaseFilter);

    url = "http://" + location.host + "/cgi-bin/getstatswrapper" + query;
    //alert("url: >" + url + "<");

    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",

        success: function(data, textStatus, request) {
            if (request.readyState == 4) {
                if (request.status == 200) {

                    if (data.error != null) {
                        updateStatus(
                            "fetching statistics ... failed: " +
                                data.error, false);
                        return;
                    }

                    updateStatus("fetching statistics ... done", false);
                    updateStatus("", false);

                    // Show context ...
                    $("#main_context_database").text(data.database);
                    $("#main_context_host").text(data.host);
                    $("#main_context_platform").text(data.platform);
                    $("#main_context_branch").text(data.branch);
                    $("#main_context_sha11").text(data.sha11);
                    $("#main_context_sha12").text(data.sha12);
                    $("#main_context_difftol").text(data.difftol);
                    $("#main_context_durtolmin").text(data.durtolmin);
                    $("#main_context_durtolmax").text(data.durtolmax);

                    setSnapshots(data.snapshots);

                    populatePbmTable("#pbmTable_all", data);
                    populatePbmTable("#pbmTable_lcRegr", data);
                    populatePbmTable("#pbmTable_lcImpr", data);

                    // Initially show the table with all benchmarks:
                    $("#select_pbmTable").attr("value", "all");
                    selectPbmTable();

                    $("#div_tsbm_border").css("display", "block");
                    $("#div_tsbm").css("display", "block");
                    $("#div_perBenchmarkStats").css("display", "block");

                    clearPlot();
                }
            }
        },

        error: function(request, textStatus, errorThrown) {
            descr = errorThrown;
            if (errorThrown == null) {
                descr = "undefined error - is the server down?";
            }
            updateStatus("fetching statistics ... error: " + descr, false);
        }

        // complete: function(request, textStatus) {
        //     alert("complete; request.status: " + request.status)
        // }

    });
}

function initPbmTable(tableSel) {
    $(tableSel).tablesorter({
        headers: {
            0: { sorter: false }, // checkbox
            1: { sorter: "mixed_numeric_desc_before_missing" }, // MS
            2: { sorter: "mixed_numeric_desc_before_missing" }, // LSD
            3: { sorter: "mixed_numeric_desc_before_missing" }, // NI
            4: { sorter: "mixed_numeric_desc_before_missing" }, // NZ
            5: { sorter: "mixed_numeric_desc_before_missing" }, // NC
            6: { sorter: "mixed_numeric_desc_before_missing" }, // MDRSE
            7: { sorter: "mixed_numeric_desc_before_missing" }, // RSEMD
            8: { sorter: "mixed_numeric_desc_before_missing" }, // QS
            9: { sorter: "mixed_numeric_desc_before_missing" }, // LC
            10: { sorter: "mixed_numeric_asc_before_missing" }, // LCDA
            11: { sorter: "mixed_numeric_desc_before_missing" }, // LCMS
            12: { sorter: "mixed_numeric_desc_before_missing" }, // LCSS
            13: { sorter: "mixed_numeric_desc_before_missing" }, // LCSS1
            14: { sorter: "mixed_numeric_desc_before_missing" }, // LCGSS
            15: { sorter: "mixed_numeric_desc_before_missing" }, // LCLSS
            16: { sorter: "mixed_numeric_desc_before_missing" }, // LCDS1
            17: { sorter: "mixed_numeric_desc_before_missing" } // LCDS2
        }
    });

    // Note: The nth-child selector below uses 1-based indexing!
    setTooltip($(tableSel).find("th:nth-child(2)"), tooltipText_ms());
    setTooltip($(tableSel).find("th:nth-child(3)"), tooltipText_lsd());
    setTooltip($(tableSel).find("th:nth-child(4)"), tooltipText_ni());
    setTooltip($(tableSel).find("th:nth-child(5)"), tooltipText_nz());
    setTooltip($(tableSel).find("th:nth-child(6)"), tooltipText_nc());
    setTooltip($(tableSel).find("th:nth-child(7)"), tooltipText_mdrse());
    setTooltip($(tableSel).find("th:nth-child(8)"), tooltipText_rsemd());
    setTooltip($(tableSel).find("th:nth-child(9)"), tooltipText_qs());
    setTooltip($(tableSel).find("th:nth-child(10)"), tooltipText_lc());
    setTooltip($(tableSel).find("th:nth-child(11)"), tooltipText_lcda());
    setTooltip($(tableSel).find("th:nth-child(12)"), tooltipText_lcms());
    setTooltip($(tableSel).find("th:nth-child(13)"), tooltipText_lcss());
    setTooltip($(tableSel).find("th:nth-child(14)"), tooltipText_lcss1());
    setTooltip($(tableSel).find("th:nth-child(15)"), tooltipText_lcgss());
    setTooltip($(tableSel).find("th:nth-child(16)"), tooltipText_lclss());
    setTooltip($(tableSel).find("th:nth-child(17)"), tooltipText_lcds1());
    setTooltip($(tableSel).find("th:nth-child(18)"), tooltipText_lcds2());

    $(tableSel).bind("sortStart",function() {
        $("#pbmTable_sortInProgress").show();
    }).bind("sortEnd",function() {
        $("#pbmTable_sortInProgress").hide();
    });
}

$(document).ready(function() {

    initTablesorter();
    initTSBMBody();

    initPbmTable("#pbmTable_all");
    initPbmTable("#pbmTable_lcRegr");
    initPbmTable("#pbmTable_lcImpr");

    var args = queryStringArgs();

    // Extract database (required):
    database = extractArg(args, "db");
    if (database == "") {
        alert("ERROR: invalid query string (empty database)");
        return;
    }

    // Extract context (required):
    host = extractArg(args, "host");
    if (host == "") {
        alert("ERROR: invalid query string (empty host)");
        return;
    }
    platform = extractArg(args, "platform");
    if (platform == "") {
        alert("ERROR: invalid query string (empty platform)");
        return;
    }
    branch = extractArg(args, "branch");
    if (branch == "") {
        alert("ERROR: invalid query string (empty branch)");
        return;
    }
    var sha11 = extractArg(args, "sha11");
    if (sha11 == "") {
        alert("ERROR: invalid query string (empty sha11)");
        return;
    }
    var sha12 = extractArg(args, "sha12");
    if (sha11 == "") {
        alert("ERROR: invalid query string (empty sha12)");
        return;
    }

    // Extract tolerances and test case filter (optional);
    var difftol = extractArg(args, "difftol");
    var durtolmin = extractArg(args, "durtolmin");
    var durtolmax = extractArg(args, "durtolmax");
    var testCaseFilter = extractArg(args, "testcasefilter");

    $("#div_tsbm_border").css("display", "none");
    $("#div_tsbm").css("display", "none");
    $("#div_perBenchmarkStats").css("display", "none");

    // Fetch per-benchmark stats and populate table:
    fetchStats(
        database, host, platform, branch, sha11, sha12, difftol, durtolmin,
        durtolmax, testCaseFilter);
});
