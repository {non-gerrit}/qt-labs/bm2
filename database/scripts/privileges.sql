-- Prevent 'bmuser' from creating objects (such as tables):
REVOKE CREATE ON SCHEMA public FROM public;
 -- even prevent creation of new schemas:
REVOKE CREATE,TEMP ON DATABASE bm FROM public;

-- NOTE: At this point, the 'bmuser' essentially cannot access the
-- 'bm' database at all. We need to explicitly add the desired privileges.

-- Define privileges for the 'bmuser':

GRANT SELECT ON upload TO bmuser;
GRANT INSERT ON upload TO bmuser;
GRANT UPDATE ON upload_id_seq TO bmuser; -- To allow auto-incrementing
                                         -- bigserial type.
GRANT SELECT ON upload_id_seq TO bmuser; -- To retrieve the latest ID.

GRANT SELECT ON host TO bmuser;
GRANT INSERT ON host TO bmuser;
GRANT UPDATE ON host_id_seq TO bmuser;

GRANT SELECT ON platform TO bmuser;
GRANT INSERT ON platform TO bmuser;
GRANT UPDATE ON platform_id_seq TO bmuser;

GRANT SELECT ON branch TO bmuser;
GRANT INSERT ON branch TO bmuser;
GRANT UPDATE ON branch_id_seq TO bmuser;

GRANT SELECT ON sha1 TO bmuser;
GRANT INSERT ON sha1 TO bmuser;
GRANT UPDATE ON sha1_id_seq TO bmuser;

GRANT SELECT ON testCase TO bmuser;
GRANT INSERT ON testCase TO bmuser;
GRANT UPDATE ON testCase_id_seq TO bmuser;

GRANT SELECT ON benchmark TO bmuser;
GRANT INSERT ON benchmark TO bmuser;
GRANT UPDATE ON benchmark_id_seq TO bmuser;

GRANT SELECT ON metric TO bmuser;
GRANT INSERT ON metric TO bmuser;
GRANT UPDATE ON metric_id_seq TO bmuser;

GRANT SELECT ON result TO bmuser;
GRANT INSERT ON result TO bmuser;
GRANT UPDATE ON result_id_seq TO bmuser;

GRANT SELECT ON context TO bmuser;
GRANT INSERT ON context TO bmuser;
GRANT UPDATE ON context_id_seq TO bmuser;

GRANT SELECT ON timeSeriesAnnotation TO bmuser;
GRANT INSERT ON timeSeriesAnnotation TO bmuser;
GRANT UPDATE ON timeSeriesAnnotation TO bmuser;
GRANT DELETE ON timeSeriesAnnotation TO bmuser;
GRANT UPDATE ON timeSeriesAnnotation_id_seq TO bmuser;

GRANT SELECT ON change TO bmuser;
GRANT INSERT ON change TO bmuser;
GRANT UPDATE ON change TO bmuser;
GRANT DELETE ON change TO bmuser;
GRANT UPDATE ON change_id_seq TO bmuser;
