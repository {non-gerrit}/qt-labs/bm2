-- Upload events:
CREATE TABLE upload
(
    id BIGSERIAL PRIMARY KEY,
    startTime TIMESTAMP NOT NULL DEFAULT now(),
    clientIPAddr TEXT NOT NULL DEFAULT inet_client_addr()
) WITH (OIDS=FALSE);
ALTER TABLE upload OWNER TO postgres;
CREATE INDEX upload_startTime_idx ON upload (startTime);
CREATE INDEX upload_clientIPAddr_idx ON upload (clientIPAddr);


-- Texts referred to by other tables:
CREATE TABLE host (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE host OWNER TO postgres;
CREATE INDEX host_value_idx ON host (value);
--
CREATE TABLE platform (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE platform OWNER TO postgres;
CREATE INDEX platform_value_idx ON platform (value);
--
CREATE TABLE branch (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE branch OWNER TO postgres;
CREATE INDEX branch_value_idx ON branch (value);
--
CREATE TABLE sha1 (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE sha1 OWNER TO postgres;
CREATE INDEX sha1_value_idx ON sha1 (value);
--
CREATE TABLE testCase (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE testCase OWNER TO postgres;
CREATE INDEX testCase_value_idx ON testCase (value);
--
CREATE TABLE benchmark (
    id BIGSERIAL PRIMARY KEY,
    value TEXT UNIQUE NOT NULL,
    testCaseId BIGINT NOT NULL REFERENCES testCase ON DELETE CASCADE
) WITH (OIDS=FALSE);
ALTER TABLE benchmark OWNER TO postgres;
CREATE INDEX benchmark_value_idx ON benchmark (value);
CREATE INDEX benchmark_testCase_idx ON benchmark (testCaseId);
--
CREATE TABLE metric (
    id BIGSERIAL PRIMARY KEY, value TEXT UNIQUE NOT NULL,
    lowerIsBetter BOOLEAN NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE metric OWNER TO postgres;
CREATE INDEX metric_value_idx ON metric (value);


-- Common data for a set of results.
CREATE TABLE context
(
    id BIGSERIAL PRIMARY KEY,
    hostId BIGINT NOT NULL REFERENCES host ON DELETE CASCADE,
    platformId BIGINT NOT NULL REFERENCES platform ON DELETE CASCADE,
    branchId BIGINT NOT NULL REFERENCES branch ON DELETE CASCADE,
    sha1Id BIGINT NOT NULL REFERENCES sha1 ON DELETE CASCADE,

    -- The timestamp is approximated with earliest known startTime:
    timestamp TIMESTAMP NOT NULL DEFAULT now(),

    UNIQUE(hostId, platformId, branchId, sha1Id)
) WITH (OIDS=FALSE);
ALTER TABLE context OWNER TO postgres;
CREATE INDEX context_host_idx ON context (hostId);
CREATE INDEX context_platform_idx ON context (platformId);
CREATE INDEX context_branch_idx ON context (branchId);
CREATE INDEX context_sha1_idx ON context (sha1Id);
CREATE INDEX context_timestamp_idx ON context (timestamp);


--Benchmark results (i.e. raw values produced from executing benchmarks):
CREATE TABLE result
(
    id BIGSERIAL PRIMARY KEY,
    contextId BIGINT NOT NULL REFERENCES context ON DELETE CASCADE,
    benchmarkId BIGINT NOT NULL REFERENCES benchmark ON DELETE CASCADE,
    value REAL NOT NULL,
    valid BOOLEAN NOT NULL,
    metricId BIGINT NOT NULL REFERENCES metric ON DELETE CASCADE,
    uploadId BIGINT NOT NULL REFERENCES upload ON DELETE CASCADE
) WITH (OIDS=FALSE);
ALTER TABLE result OWNER TO postgres;
CREATE INDEX result_context_idx ON result (contextId);
CREATE INDEX result_benchmark_idx ON result (benchmarkId);
CREATE INDEX result_metric_idx ON result (metricId);
CREATE INDEX result_valid_idx ON result (valid);


-- Time series annotations:
CREATE TABLE timeSeriesAnnotation
(
    id BIGSERIAL PRIMARY KEY,

    -- Time series key:
    hostId BIGINT NOT NULL REFERENCES host ON DELETE CASCADE,
    platformId BIGINT NOT NULL REFERENCES platform ON DELETE CASCADE,
    branchId BIGINT NOT NULL REFERENCES branch ON DELETE CASCADE,
    benchmarkId BIGINT NOT NULL REFERENCES benchmark ON DELETE CASCADE,
    metricId BIGINT NOT NULL REFERENCES metric ON DELETE CASCADE,

    -- Annotations:
    note TEXT NOT NULL,

    UNIQUE (hostId, platformId, branchId, benchmarkId, metricId)
) WITH (OIDS=FALSE);
ALTER TABLE timeSeriesAnnotation OWNER TO postgres;
CREATE INDEX tsanno_host_idx ON timeSeriesAnnotation (hostId);
CREATE INDEX tsanno_platform_idx ON timeSeriesAnnotation (platformId);
CREATE INDEX tsanno_branch_idx ON timeSeriesAnnotation (branchId);
CREATE INDEX tsanno_benchmark_idx ON timeSeriesAnnotation (benchmarkId);
CREATE INDEX tsanno_metric_idx ON timeSeriesAnnotation (metricId);


--Time series changes:
CREATE TABLE change
(
    id BIGSERIAL PRIMARY KEY,

    benchmarkId BIGINT NOT NULL REFERENCES benchmark ON DELETE CASCADE,
    testCaseId BIGINT NOT NULL REFERENCES testCase ON DELETE CASCADE,
    metricId BIGINT NOT NULL REFERENCES metric ON DELETE CASCADE,

    hostId BIGINT NOT NULL REFERENCES host ON DELETE CASCADE,
    platformId BIGINT NOT NULL REFERENCES platform ON DELETE CASCADE,
    branchId BIGINT NOT NULL REFERENCES branch ON DELETE CASCADE,

    sha1Id BIGINT NOT NULL REFERENCES sha1 ON DELETE CASCADE,
    timestamp INTEGER NOT NULL, -- First upload timestamp (UTC)

    regression BOOLEAN NOT NULL, -- Regression or improvement

    score REAL NOT NULL,
    premature_score REAL NOT NULL,

    UNIQUE (benchmarkId, metricId, hostId, platformId, branchId, sha1id)
) WITH (OIDS=FALSE);
ALTER TABLE change OWNER TO postgres;
CREATE INDEX change_benchmark_idx ON change (benchmarkId);
CREATE INDEX change_testcase_idx ON change (testCaseId);
CREATE INDEX change_metric_idx ON change (metricId);
CREATE INDEX change_host_idx ON change (hostId);
CREATE INDEX change_platform_idx ON change (platformId);
CREATE INDEX change_branch_idx ON change (branchId);
CREATE INDEX change_sha1_idx ON change (sha1Id);
CREATE INDEX change_timestamp_idx ON change (timestamp);
CREATE INDEX change_regression_idx ON change (regression);
CREATE INDEX change_score_idx ON change (score);
CREATE INDEX change_premature_score_idx ON change (premature_score);
