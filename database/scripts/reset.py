#!/usr/bin/env python

import sys
from subprocess import Popen, PIPE
import string

"""
NOTE: This script should be run as the 'postgres' role.

WARNING: The script wipes everything from the database, so use it with care!
"""

# --- Global functions -------------------------------------------------

def runCommand(cmd, exitOnFail = True):
    print "running command >%s< ..." % string.join(cmd, " "),
    p = Popen(cmd, stdout = PIPE, stderr = PIPE)
    stdout, stderr = p.communicate()
    if (p.returncode != 0):
        print "FAIL:"
        print "  return code:", p.returncode
        print "  stdout: >%s<" % stdout.strip()
        print "  stderr: >%s<" % stderr.strip()
        if (exitOnFail):
            print "exiting ..."
            sys.exit(1)
        else:
            print "CONTINUING ..."
    else:
        print "PASS"


# --- Main program -------------------------------------------------

# Ensure the intention of running this script.
reply = raw_input(
    "WARNING: Everything in the current database will be deleted!"
    " Continue [y/N]?").strip().lower()
if (reply != "y" and reply != "yes"):
    sys.exit(1)

database = "bm"
user = "bmuser"
owner = "postgres"
tabledefs = "tabledefs.sql"
plpythonfuncs = "plpythonfuncs.sql"
privileges = "privileges.sql"

# Drop the database (if any).
runCommand(["dropdb", database], False)

# Drop the user (if any).
runCommand(["dropuser", user], False)

# (Re)create the user.
runCommand(
    ["createuser", "--no-createdb", "--login", "--no-inherit",
     "--no-createrole", "--no-superuser", "--no-password", user])

# (Re)create the database.
runCommand(["createdb", "-O", owner, database])

# Load table definitions.
runCommand(["psql", "-U", owner, "-d", database, "-f", tabledefs])

# Load PL/Python functions. ...
runCommand(["createlang", "-U", owner, "-d", database, "plpythonu"])
runCommand(["psql", "-U", owner, "-d", database, "-f", plpythonfuncs])

# Load PL/pgSQL functions. ...
runCommand(["createlang", "-U", owner, "-d", database, "plpgsql"])
runCommand(["psql", "-U", owner, "-d", database, "-f", plpgsqlfuncs])

# Define user privileges.
# (Note: remote client access is defined separately in pg_hba.conf)
runCommand(["psql", "-U", owner, "-d", database, "-f", privileges])
