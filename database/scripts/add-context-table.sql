--
-- Upgrade script from previous context-less schema
--

-- Add new tables
CREATE TABLE context
(
    id BIGSERIAL PRIMARY KEY,

    -- Environment:
    hostId BIGINT NOT NULL REFERENCES host ON DELETE CASCADE,
    platformId BIGINT NOT NULL REFERENCES platform ON DELETE CASCADE,

    -- Revision:
    branchId BIGINT NOT NULL REFERENCES branch ON DELETE CASCADE,
    sha1Id BIGINT NOT NULL REFERENCES sha1 ON DELETE CASCADE,

    -- Timestamp: (approximated with earliest known startTime)
    timestamp TIMESTAMP NOT NULL,

    UNIQUE(hostId, platformId, branchId, sha1Id)
) WITH (OIDS=FALSE);
ALTER TABLE context OWNER TO postgres;

-- Import existing data
INSERT INTO context (hostId, platformId, branchId, sha1Id, timestamp)
    SELECT hostId, platformId, branchId, sha1Id, min(startTime) AS timestamp
        FROM
            (SELECT DISTINCT hostId, platformId, branchId, sha1Id, uploadId FROM result) AS dummy
            JOIN upload ON uploadId = upload.id
        GROUP BY hostId, platformId, branchId, sha1Id;

-- Add new indexes
CREATE INDEX context_host_idx ON result (hostId);
CREATE INDEX context_platform_idx ON result (platformId);
CREATE INDEX context_branch_idx ON result (branchId);
CREATE INDEX context_sha1_idx ON result (sha1Id);

-- Update result table schema -- step 1 of 3
ALTER TABLE result ADD COLUMN contextId BIGINT REFERENCES context ON DELETE CASCADE;

UPDATE result
    SET contextId = context.id
    FROM context
    WHERE context.hostId = result.hostId
        AND context.platformId = result.platformId
        AND context.branchId = result.branchId
        AND context.sha1Id = result.sha1Id;

-- Drop obsolete indexes
DROP INDEX result_host_idx;
DROP INDEX result_platform_idx;
DROP INDEX result_branch_idx;
DROP INDEX result_sha1_idx;

-- Update result table schema -- step 2 of 3
ALTER TABLE result
    ALTER COLUMN contextId SET NOT NULL;

-- Update result table schema -- step 3 of 3
ALTER TABLE result
    DROP COLUMN hostId,
    DROP COLUMN platformId,
    DROP COLUMN branchId,
    DROP COLUMN sha1Id;

-- Add new indexes
CREATE INDEX result_context_idx ON result (contextId);

