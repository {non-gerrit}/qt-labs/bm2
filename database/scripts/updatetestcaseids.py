#!/usr/bin/env python

"""
This script updates the testCaseId attribute of all rows in the benchmark table.
"""

import sys
from dbaccess import setDatabase, execQuery, commit
from misc import findOrInsertId, getOptions, benchmarkToComponents

# --- BEGIN Global functions ----------------------------------------------
def printUsage():
    sys.stderr.write(
        "usage: " + sys.argv[0] + " [--dbhost H] [--dbport P] --db D\n")
# --- END Global functions ----------------------------------------------


# --- BEGIN Main program ----------------------------------------------

options, http_get = getOptions()

if not ("db" in options):
    printUsage()
    sys.exit(1)

setDatabase(
    options["dbhost"] if "dbhost" in options else None,
    options["dbport"] if "dbport" in options else None,
    options["db"])

for benchmark_id, benchmark in execQuery(
    "SELECT id, value FROM benchmark;", ()):

    test_case, test_function, data_tag = (
        benchmarkToComponents(benchmark))
    test_case_id = findOrInsertId("testCase", test_case)

    execQuery(
        "UPDATE benchmark"
        " SET testCaseId = %s"
        " WHERE id = %s", (test_case_id, benchmark_id), False)

commit()

# --- END Main program ----------------------------------------------
