-- Inserts or updates a row in the 'timeSeriesAnnotation' table.
CREATE OR REPLACE FUNCTION merge_timeSeriesNote(
    hostId_ BIGINT, platformId_ BIGINT, branchId_ BIGINT,
    benchmarkId_ BIGINT, metricId_ BIGINT, note_ TEXT) RETURNS VOID AS
$$
BEGIN
    BEGIN
        INSERT INTO timeSeriesAnnotation(
            hostId, platformId, branchId, benchmarkId, metricId, note)
        VALUES (
            hostId_, platformId_, branchId_, benchmarkId_, metricId_, note_);
        RETURN;
    EXCEPTION WHEN unique_violation THEN
        UPDATE timeSeriesAnnotation
        SET hostId = hostId_, platformId = platformId_, branchId = branchId_,
            note = note_
        WHERE hostId = hostId_
          AND platformId = platformId_
          AND branchId = branchId_
          AND benchmarkId = benchmarkId_
          AND metricId = metricId_;
    END;
END;
$$
LANGUAGE plpgsql;
